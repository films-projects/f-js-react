# Endpoints

The following describes the endpoints used by the frontend,

## Pages

-   `/login`
-   `/search`
-   `/upcoming`

## List

-   [`/content/{id}/delete`](#content)
-   [`/content/{id}/update?title=<title>&year=<year>&rating=<rating>&watching=<watching&specialInterest=<specialInterest>`](#content)
-   [`/content/{id}/watch/{provider}`](#content)
-   [`/content/upcoming`](#content)
-   [`/contentInfo/{id}/connect/{externalId}`](#contentinfo)
-   [`/contentInfo/{id}/disconnect`](#contentinfo)
-   [`/contentInfo/{id}/full`](#contentinfo)
-   [`/contentInfo/{id}/refresh`](#contentinfo)
-   [`/contentInfo/search/{type}/{title}`](#contentinfo)
-   [`/film/new`](#film)
-   [`/film/newFromExternal/{externalId}`](#film)
-   [`/film/suggest`](#film)
-   [`/film/update`](#film)
-   [`/search/do`](#search)
-   [`/search/favorites`](#search)
-   [`/search/favorites/add`](#search)
-   [`/search/favorites/remove`](#search)
-   [`/series/{id}/episodeInfo`](#series)
-   [`/series/new`](#series)
-   [`/series/newFromExternal/{externalId}`](#series)
-   [`/series/upcoming`](#series)
-   [`/series/update`](#series)
-   [`/user/load`](#user)

## Arguments and response values

### Login

Frontend uses a login form with the following parameters

-   `username`
-   `password`
-   `rememberme`

### Search

-   `/search/do`

    -   Method: GET
    -   query params
        -   title
            -   include only content containing the given string in the title
            -   type: optional string
        -   yearFrom
            -   include only content that was released no earlier than this year
            -   type: optional int
        -   yearTo
            -   include only content that was released no later than this year
            -   type: optional int
        -   anticipated
            -   include only content with this `anticipated` value
            -   type: optional boolean
        -   myRating
            -   include only content having this rating
            -   type: optional list of int
        -   type
            -   the type of content, `FILM` or `SERIES`
            -   type: optional string
        -   watching
            -   whether I'm currently watching this content, series only
            -   type: optional boolean
        -   minRating
            -   include only content having at minimum this external rating
            -   type: optional double
        -   status
            -   include only series that have this series status
            -   type: optional series status
    -   Response: [SearchResponse](./endpoints.md#SearchResponse)

-   `/search/favorites`

    -   Method: GET
    -   Response: [[SearchQuery](#SearchQuery)]

-   `/search/favorites/add`

    -   Method: POST
    -   Params:
        -   query: string
    -   Response: [SearchQuery](#SearchQuery)

-   `/search/favorites/remove`
    -   Method: POST
    -   Params:
        -   query: string
    -   Response: [[SearchQuery](#SearchQuery)]

### Content

Endpoints concerning content, where it is not important what type the content is

-   `/content/{id}/update?title=<title>&year=<year>&rating=<rating>&watching=<watching&specialInterest=<specialInterest>`

    -   Updates the given properties of the content
    -   Params:
        -   id: int
        -   title: optional string
        -   year: optional int
        -   rating: optional int
        -   watching: optional boolean
        -   specialInterest: optional boolean
    -   Method: POST
    -   Response: none

-   `/content/{id}/delete`

    -   Deletes the content identified by `{id}`
    -   Method: `POST`
    -   Response: none

-   `/content/{id}/watch/{provider}`

    -   Redirects the user to the content identified by `id` on streaming platform `provider`
    -   Method: `GET`
    -   Response: redirect or error response

-   `/content/upcoming`
    -   Retrieves movies that will be released soon and have been released recently, and info about the most recent and
        next episodes for series for which `seen = true`
    -   Method: `GET`
    -   Params:
        -   `days` (optional)
            -   the period to search in for movies (before/after "now")
            -   type: int
    -   response: [object of upcoming content](#upcoming-content)

### Film

Endpoints for films specifically

-   `/film/new`

    -   Adds a new film
    -   Method: POST
    -   Body: `Film` object
    -   Response: [new-content object](./endpoints.md#new-content)

-   `/film/newFromExternal/{externalId}`

    -   Adds a new film by grabbing the information from an external source
    -   Method: POST
    -   Path variables
        -   `externalId` the third party id of the resource to copy from
    -   Response: [new-content-from-external object](./endpoints.md#new-content-from-external)

-   `/film/suggest`
    -   Gets a suggestion for a film to watch, based on provided filters
    -   Method: GET
    -   query params
        -   minRating
            -   type: optional double
        -   seen
            -   type: boolean
        -   anticipated
            -   type: optional boolean
        -   watchOptions
            -   type: optional list of string
    -   Response: [content info object](./endpoints.md#full-content-info)

### Series

Endpoints for series specifically

-   `/series/{id}/episodeInfo`
    -   Provides info about the most recently aired, and next to be aired episodes of this series
    -   Params:
        -   id: int
    -   Method: `GET`
    -   Response: [LastAndNextEpisode object](#lastandnextepisode)
-   `/series/new`

    -   Adds a new series
    -   Method: `POST`
    -   Body: `Series` object
    -   Response: [new content object](./endpoints.md#new-content)

-   `/series/newFromExternal/{externalId}`

    -   Adds a new series by grabbing the information from an external source
    -   Method: `POST`
    -   Path variables
        -   `externalId` the third party id of the resource to copy from
    -   Response: [new-content-from-external object](./endpoints.md#new-content-from-external)

-   `/series/upcoming`
    -   Retrieves movies that will be released soon and have been released recently, and info about the most recent and
        next episodes for series for which `seen = true`
    -   Method: `GET`
    -   Params:
        -   `days` (optional)
            -   the period to search in for movies (before/after "now")
            -   type: int
    -   response: [object of upcoming content](#upcoming-content)

### ContentInfo

Endpoints concerning the content info, i.e. info about ratings, images, where to watch, etc.

-   `/contentInfo/search/{type}/{title}`

    -   Searches for available content info for content of type `type` with title `title`
    -   Method: GET
    -   Response: List of [content info search results](./endpoints.md#content-info-search-result)

-   `/contentInfo/connect/{id}/{externalId}`

    -   Connects the info identified by `{externalId}` from an external platform to our content identified by `{id}`
    -   Method: POST
    -   Response: [content info object](./endpoints.md#content-info)

-   `/contentInfo/disconnect/{id}`

    -   Disconnects the external info for content identified by `{id}`
    -   Method: POST
    -   Response: [slide object](./endpoints.md#slide)

-   `/contentInfo/full/{id}`

    -   Returns all info we can gather for content identified by `{id}`
    -   Method: GET
    -   Response: [content info object](./endpoints.md#full-content-info)

-   `/contentInfo/refresh/{id}`

    -   Refreshes the external info for content identified by `{id}`, re-retrieving all information that was stored (such
        as the poster path, which may have become invalid)
    -   Method: POST
    -   Response: [content info object](./endpoints.md#content-info)

-   `/contentInfo/search/{type}/{title}`

    -   Searches external resources for content of the given type and matching the given title
    -   Method: POST
    -   Response: [content info search result](./endpoints.md#content-info-search-result)

## DTOs

### SearchParams

Object containing search params to search content

### New content

Response object after adding new content.

-   id
    -   id of the newly added content
    -   type: int
-   type
    -   type of the newly added content, either `FILM` or `SERIES`
    -   type: string
-   title
    -   title of the newly added content
    -   type: string

### New content from external

Response object after adding new content.

-   title
    -   title of the newly added content
    -   type: string

### SearchResponse

A response to a search request, containing

-   timestamp
    -   the timestamp the server sent back the response, used to show only the results for the latest search query
-   results
    -   content
        -   the search results
        -   type: List of [Slide](./endpoints.md#Slide)
    -   number
        -   the current page (zero based)
        -   type: int
    -   totalPages
        -   the total number of pages available for a search with the current parameters
        -   type: int
    -   totalElements
        -   the total number of results for a search with the current parameters
        -   type: int

### SearchQuery

An entity containing a search query, both as query string and as map of parameters to list of values.

-   query
    -   the query as querystring
    -   type: string
-   params
    -   the query as map of parameters and a list of their values
    -   type: <string: [string]>

### Slide

A complete representation of some content

-   TODO

### Full Content Info

Object containing the content info for some content

-   id
    -   id of the content
    -   type: int
-   title
    -   title of the content
    -   type: string
-   year
    -   the year the content was released
    -   type: string
-   seen
    -   whether I've seen the content
    -   type: boolean
-   myRating
    -   my rating of this content, going from 0 (no rating) up to 5
    -   type: int
-   overview
    -   synopsis of the content
    -   type: optional string
-   posterImage
    -   url of a portrait image of the content
    -   type: optional string
-   imdbId
    -   the id for this content on the IMDB platform
    -   type: optional string
-   imdbRating
    -   IMDB rating of the content
    -   type: optional double
-   runtime
    -   the runtime for this content, if it is a movie
    -   type: int, default 0
-   status
    -   the production status for this content, if it is a series
    -   type: optional string
-   episodeRuntime
    -   the duration of a single episode of this content, if it is a series
    -   type: int, default 0
-   numEpisodes
    -   the number of episodes for this content, if it is a series
    -   type: int, default 0
-   streamingPlatforms
    -   names of platforms where this content can be watched
    -   type: List of string, default empty list
    -   values supported by frontend: `NETFLIX`, `APPLE_TV`, `AMAZON_PRIME`, `HBO_MAX`
-   cast
    -   the names of the case
    -   type: list of string, default empty
-   genres
    -   the genres of the content
    -   type: list of string, default empty
-   originalLanguage
    -   the original language of the content
    -   type: optional string
-   similarTitles
    -   names of content that are similar to this content
    -   type: list of string, default empty
-   episodeRuntime
    -   the duration of a single episode of this content, if it is a series
    -   type: int, default 0
-   numEpisodes
    -   the number of episodes for this content, if it is a series
    -   type: int, default 0
-   lastRefresh
    -   the date of the last refresh of the content details
    -   type: optional string in format `YYYY-MM-DD`

### Content Info

Object containing the content info for some content

-   overview
    -   synopsis of the content
    -   type: optional string
-   imdbRating
    -   IMDB rating of the content
    -   type: optional double
-   posterImage
    -   url of a portrait image of the content
    -   type: optional string
-   imdbId
    -   the id for this content on the IMDB platform
    -   type: optional string
-   runtime
    -   the runtime for this content, if it is a movie
    -   type: int, default 0
-   status
    -   the production status for this content, if it is a series
    -   type: optional string
-   episodeRuntime
    -   the duration of a single episode of this content, if it is a series
    -   type: int, default 0
-   numEpisodes
    -   the number of episodes for this content, if it is a series
    -   type: int, default 0
-   streamingPlatforms
    -   names of platforms where this content can be watched
    -   type: List of string, default empty list
    -   values supported by frontend: `NETFLIX`, `APPLE_TV`, `AMAZON_PRIME`, `HBO_MAX`

### Content info search result

Result after searching for content info for some content.

-   externalId
    -   id of the content on the external platform
    -   type: int
-   title
    -   title of the content on the external platform
    -   type: string
-   posterImage: String
    -   url of a poster image for the content
    -   type: optional string
-   overview:
    -   synopsis of the content
    -   type: optional string
-   releaseDate
    -   release date of this content
    -   type: optional string
-   contentHasBeenAdded
    -   whether other content has been content with this external resource already
    -   type: optional boolean

### Upcoming content

Object containing upcoming movies, recently released movies (for which `seen = false`), and information about the most
recent and next-to-be aired episodes for series for this `watching = true`

-   series
    -   type: array of [SeriesAndEpisodes](#seriesandepisodes)
-   upcomingMovies
    -   type: list of [UpcomingContentDTO](#upcomingcontentdto)
-   recentMovies
    -   type: list of [UpcomingContentDTO](#upcomingcontentdto)

### SeriesAndEpisodes

Information about a series and its most recent and next episodes

-   series
    -   Information about the series itself
    -   type: [UpcomingContentDTO](#upcomingcontentdto)
-   lastEpisode
    -   information about the most recently aired episode for this series
    -   type: optional [EpisodeInfo](#episodeinfo)
-   nextEpisode
    -   information about the next episode to be aired for this series
    -   type: optional [EpisodeInfo](#episodeinfo)

### LastAndNextEpisode

Information about a series' recent and next episodes

-   lastEpisode
    -   information about the most recently aired episode for this series
    -   type: optional [EpisodeInfo](#episodeinfo)
-   nextEpisode
    -   information about the next episode to be aired for this series
    -   type: optional [EpisodeInfo](#episodeinfo)

### Upcoming Series

Information about a series and its upcoming episode

-   title
    -   title of the series
    -   type: string
-   episode
    -   the upcoming episode
    -   type: [EpisodeInfo](#episodeinfo)

### EpisodeInfo

Information about an episode of a series

-   season
    -   the season the episode is part of
    -   type: int
-   episode
    -   the episode number in the season
    -   type: int
-   airDate
    -   the air date of the episode
    -   type: string in the format "YYYY-MM-DD"
-   isFinale
    -   whether the episode is the season finale
    -   type: boolean

### UpcomingContentDTO

Object containing information for upcoming content

-   id
    -   id of the content
    -   type: int
-   title
    -   title of the content
    -   type: string
-   posterUrl
    -   url of the poster image of the content
    -   type: optional string
-   releaseDate
    -   the release date of the content
    -   type: optional string in the format "YYYY-MM-DD"
