# Images

Here a list of the images used by the frontend, and expected to be provided by the backend:

-   /img
    -   /external
        -   `imdb.svg` - used when showing rating
        -   `netflix.avg` - shown when content can be viewed on Netflix
        -   `apple.avg` - shown when content can be viwed on Apple TV Plus
        -   `prime.avg` - shown when content can be viewed on Amazon Prime Video
        -   `hbomax.svg` - shown when content can be viewed on HBO Max
        -   `showtime.svg` - shown when content can be viewed on SkyShowtime
    -   /alert - used when showing alerts
        -   `inform.png`
        -   `success.svg`
        -   `error.png`
    -   /icons
        -   `anticipated-no.svg`
        -   `anticipated-yes.svg`
        -   `delete.svg`
        -   `edit.svg`
        -   `info.svg` - used for quick button to show content info
        -   `rating-0.svg` - the emojis used to represent my rating
        -   `rating-1.png`
        -   `rating-2.png`
        -   `rating-3.png`
        -   `rating-4.png`
        -   `rating-5.png`
        -   `tv.svg`
        -   `watching-yes.png`
        -   `watching-no.png`
