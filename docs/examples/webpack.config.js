const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path2 = require("path");

module.exports = {
    entry: {
        login: "./frontend/js/pages/login.js",
        search: "./frontend/js/pages/search.js",
    },
    output: {
        path: path2.resolve(__dirname, "./outputdir"),
        filename: "js/[name].js",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                },
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                            modules: {
                                localIdentName: "[name]__[local]___[hash:base64]",
                            },
                        },
                    },
                    {
                        loader: "sass-loader",
                    },
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "./css/[name].css",
            chunkFilename: "[id].css",
        }),
    ],
};
