export const CONTENT_INFO_DISCONNECT = contentId => `/contentInfo/${contentId}/disconnect`;
export const CONTENT_INFO_FIND_BY_TITLE = (type, title) => `/contentInfo/search/${type}/${title}`;
export const CONTENT_INFO_CONNECT = (contentId, externalId) => `/contentInfo/${contentId}/connect/${externalId}`;
export const CONTENT_INFO_REFRESH = contentId => `/contentInfo/${contentId}/refresh`;
export const SERIES_EPISODE_INFO = contentId => `/series/${contentId}/episodeInfo`;
export const FULL_CONTENT_INFO = contentId => `/contentInfo/${contentId}/full`;
