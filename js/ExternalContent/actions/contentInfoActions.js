import {
    connectContentInfoReq,
    disconnectContentInfoReq,
    findContentInfoByTitleReq,
    getSeriesInfoReq,
    refreshContentInfoReq,
} from "../requests/contentInfo";
import { closeDialogs } from "../../Common/actions/dialogActions";
import { replaceContent, updateContent } from "../../Content/actions";
import { addNotification, addNotificationFromError } from "../../Common/actions/notificationActions";
import { NOTIFICATION_TYPE_SUCCESS } from "../../Common/constants/notificationConstants";
import { getFullContentReq } from "../../Content/requests";

export const disconnectContentInfo = id => dispatch =>
    disconnectContentInfoReq(id)
        .then(json => {
            dispatch(replaceContent(json));
            dispatch(closeDialogs());
            dispatch(addNotification("The content into has been disconnected", NOTIFICATION_TYPE_SUCCESS));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const loadContentInfoSuggestionsByTitle = (type, title) => dispatch =>
    findContentInfoByTitleReq(type, title).catch(e => dispatch(addNotificationFromError(e)));

export const contentInfoConnect = (filmId, tmdbId) => dispatch =>
    connectContentInfoReq({ filmId, tmdbId })
        .then(json => {
            dispatch(updateContent(Object.assign({ id: filmId, connected: true }, json)));
            dispatch(closeDialogs());
            dispatch(addNotification("The content info has been connected", NOTIFICATION_TYPE_SUCCESS));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const contentInfoRefresh = (contentId, callback) => dispatch =>
    refreshContentInfoReq(contentId)
        .then(json => {
            dispatch(updateContent(Object.assign({ id: contentId }, json)));
            dispatch(addNotification("The content info has been refreshed", NOTIFICATION_TYPE_SUCCESS));
            callback(json);
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const getSeriesEpisodes = (contentId, callback) => dispatch =>
    getSeriesInfoReq(contentId)
        .then(json => callback(json))
        .catch(e => dispatch(addNotificationFromError(e)));

export const getFullContent = (contentId, callback) => dispatch =>
    getFullContentReq(contentId)
        .then(json => callback(json))
        .catch(e => dispatch(addNotificationFromError(e)));
