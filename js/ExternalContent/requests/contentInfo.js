import { get, getJson, post } from "../../Common/util/defaultRequest";
import {
    CONTENT_INFO_DISCONNECT,
    CONTENT_INFO_CONNECT,
    CONTENT_INFO_REFRESH,
    CONTENT_INFO_FIND_BY_TITLE,
    SERIES_EPISODE_INFO,
} from "../constants/contentInfoEndpoints";

export const disconnectContentInfoReq = id => post(CONTENT_INFO_DISCONNECT(id)).then(res => res.json());

export const findContentInfoByTitleReq = (type, title) =>
    get(CONTENT_INFO_FIND_BY_TITLE(type, title)).then(res => res.json());

export const connectContentInfoReq = ({ filmId, tmdbId }) =>
    post(CONTENT_INFO_CONNECT(filmId, tmdbId)).then(res => res.json());

export const refreshContentInfoReq = filmId => post(CONTENT_INFO_REFRESH(filmId)).then(res => res.json());

export const getSeriesInfoReq = seriesId => getJson(SERIES_EPISODE_INFO(seriesId));
