import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Dialog from "../../Common/dialogs/AbstractDialog";
import ManualConnect from "./ManualConnect";

import { contentInfoConnect, loadContentInfoSuggestionsByTitle } from "../actions/contentInfoActions";

import ContentInfoConnectItem from "./ContentInfoConnectItem";
import LoadingBar from "../../Common/components/LoadingBar";

import styles from "./ConnectContentInfoDialog.scss";
import connectStyles from "../../ExternalContent/dialogs/ContentInfoConnectItem.scss";

class ConnectContentInfoDialog extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            data: [],
        };
    }

    componentDidMount() {
        const { loadContentInfoSuggestionsByTitle, type, title } = this.props;
        loadContentInfoSuggestionsByTitle(type.toUpperCase(), title).then(data =>
            this.setState({ data, loading: false }),
        );
    }

    render() {
        const { id, contentInfoConnect } = this.props;
        const { loading, data } = this.state;
        const body = loading ? (
            <LoadingBar />
        ) : (
            (data || []).map(externalResource => (
                <ContentInfoConnectItem
                    key={externalResource.externalId}
                    title={externalResource.title}
                    status={externalResource.status}
                    overview={externalResource.overview}
                    releaseDate={externalResource.releaseDate}
                    posterImage={externalResource.posterImage}
                    buttons={
                        <button
                            className={connectStyles.btnConnect}
                            onClick={() => contentInfoConnect(id, externalResource.externalId)}
                        >
                            Connect
                        </button>
                    }
                />
            ))
        );
        return (
            <Dialog className={styles.dialog} title="Add content info">
                <ManualConnect contentId={id} connect={contentInfoConnect} />
                {body}
            </Dialog>
        );
    }
}

ConnectContentInfoDialog.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    contentInfoConnect: PropTypes.func.isRequired,
    loadContentInfoSuggestionsByTitle: PropTypes.func.isRequired,
};

ConnectContentInfoDialog.dialogName = "DIALOG_CONNECT_CONTENT_INFO";

export default connect(null, dispatch =>
    bindActionCreators(
        {
            contentInfoConnect: contentInfoConnect,
            loadContentInfoSuggestionsByTitle: loadContentInfoSuggestionsByTitle,
        },
        dispatch,
    ),
)(ConnectContentInfoDialog);
