import React from "react";
import PropTypes from "prop-types";

import styles from "./ManualConnect.scss";

class ManualConnect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { contentId, connect } = this.props;
        const { id } = this.state;
        return (
            <div className={styles.manual}>
                <input
                    type="text"
                    value={id || ""}
                    onChange={e => this.setState({ id: e.target.value })}
                    placeholder="TMDB id or IMDB id"
                />
                <button
                    className="dialog-connect-manual-btn button-type-generic"
                    onClick={() => connect(contentId, id)}
                >
                    Connect
                </button>
            </div>
        );
    }
}

ManualConnect.propTypes = {
    contentId: PropTypes.number.isRequired,
    connect: PropTypes.func.isRequired,
};

export default ManualConnect;
