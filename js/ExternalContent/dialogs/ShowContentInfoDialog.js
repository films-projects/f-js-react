import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Dialog from "../../Common/dialogs/AbstractDialog";

import {
    contentInfoRefresh,
    disconnectContentInfo,
    getFullContent,
    getSeriesEpisodes,
} from "../actions/contentInfoActions";

import styles from "./ShowContentInfoDialog.scss";
import FullContentInfo from "./FullContentInfo";
import EpisodeContainer from "../../UpcomingContent/components/card/episodes/EpisodeContainer";
import { BUTTON_TYPE_GENERIC, BUTTON_TYPE_WARN } from "../../Common/constants/buttonTypeConstants";
import { closeAllDialogs } from "../../Common/actions/dialogActions";

class ShowContentInfoDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = { content: {}, nextEpisode: null, lastEpisode: null, loaded: false };

        this.refreshContent = this.refreshContent.bind(this);
    }

    componentDidMount() {
        const { contentId } = this.props;
        this.props.getFullContent(contentId, content => {
            this.setState({ content, loaded: true });
            if (content.type === "series") {
                this.props.getEpisodeInfo(contentId, data => this.setState(data));
            }
        });
    }

    refreshContent() {
        const { content } = this.state;
        this.props.refreshContentInfo(content.id, update =>
            this.setState({ content: Object.assign({}, content, update) }),
        );
    }

    render() {
        const { lastEpisode, nextEpisode, content, loaded } = this.state;

        if (!loaded) {
            return <></>;
        }

        if (!content.id) {
            this.props.closeDialog();
            this.setState({ loaded: false });
        }
        return (
            <Dialog
                className={styles.dialog}
                title=""
                leftText="Close"
                leftButtonType={BUTTON_TYPE_GENERIC}
                centerText="Disconnect"
                centerButtonType={BUTTON_TYPE_WARN}
                centerAction={() => this.props.disconnectContentInfo(content.id)}
                rightText="Refresh"
                rightButtonType={BUTTON_TYPE_GENERIC}
                rightAction={() => this.refreshContent(content.id)}
            >
                <FullContentInfo content={content} lastEpisode={lastEpisode} nextEpisode={nextEpisode} />

                {(lastEpisode || nextEpisode) && (
                    <div className={styles.episodes}>
                        <EpisodeContainer
                            lastEpisode={lastEpisode}
                            nextEpisode={nextEpisode}
                            episodeClass={styles.episode}
                        />
                    </div>
                )}
            </Dialog>
        );
    }
}

ShowContentInfoDialog.propTypes = {
    contentId: PropTypes.number.isRequired,
    disconnectContentInfo: PropTypes.func.isRequired,
    refreshContentInfo: PropTypes.func.isRequired,
    getEpisodeInfo: PropTypes.func.isRequired,
    getFullContent: PropTypes.func.isRequired,
    closeDialog: PropTypes.func.isRequired,
};

ShowContentInfoDialog.dialogName = "DIALOG_SHOW_CONTENT_INFO";

export default connect(undefined, {
    disconnectContentInfo: disconnectContentInfo,
    refreshContentInfo: contentInfoRefresh,
    getEpisodeInfo: getSeriesEpisodes,
    getFullContent: getFullContent,
    closeDialog: closeAllDialogs,
})(ShowContentInfoDialog);
