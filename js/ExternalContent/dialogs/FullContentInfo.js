import React from "react";
import PropTypes from "prop-types";
import { buildStyles, CircularProgressbar } from "react-circular-progressbar";
import * as countries from "country-flag-icons/string/3x2";
import { getLanguage } from "language-flag-colors";

import styles from "./FullContentInfo.scss";

import Rating from "../../Content/components/Rating";
import StreamingPlatforms from "../../Content/components/StreamingPlatforms";
import { DotSeparatedList, DotSeparator } from "../../Common/components/Separator";
import { StatusIcon } from "../../Content/components/slide/bottom/StatusIcons";
import { getRelativeDays } from "../../Common/util/getRelativeTime";

const Pill = ({ children }) => <span className={styles.pill}>{children}</span>;

Pill.propTypes = {
    children: PropTypes.any.isRequired,
};

class FullContentInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showFullCast: false, showSimilar: false };
        this.toggleCast = this.toggleCast.bind(this);
        this.toggleSimilar = this.toggleSimilar.bind(this);
    }

    toggleCast() {
        const { showFullCast } = this.state;
        this.setState({ showFullCast: !showFullCast });
    }

    toggleSimilar() {
        const { showSimilar } = this.state;
        this.setState({ showSimilar: !showSimilar });
    }

    render() {
        const { content } = this.props;
        const { showFullCast } = this.state;
        const {
            overview,
            title,
            year,
            posterImage,
            cast,
            imdbId,
            imdbRating,
            streamingPlatforms,
            genres,
            originalLanguage,
            status,
            lastRefresh,
        } = content;

        let castPart = <>No info</>;
        if (cast) {
            if (showFullCast) {
                castPart = (
                    <>
                        <DotSeparatedList listItems={[...cast].splice(0, 10)} />
                        {cast.length > 3 && (
                            <span onClick={this.toggleCast} className={styles.moreCast}>
                                Show less
                            </span>
                        )}
                    </>
                );
            } else {
                castPart = (
                    <>
                        <DotSeparatedList listItems={[...cast].splice(0, 3)} />
                        {cast.length > 3 && (
                            <span onClick={this.toggleCast} className={styles.moreCast}>
                                Show more
                            </span>
                        )}
                    </>
                );
            }
        }

        let languageSvg = null;
        if (originalLanguage) {
            const language = getLanguage(originalLanguage);
            languageSvg = countries[language.countryCode.toUpperCase()];
        }

        let durationFragment = null;
        if (content.numEpisodes && content.episodeRuntime) {
            durationFragment = (
                <Pill>
                    <img src="/img/icons/time.svg" className={styles.iconTime} />
                    {`${content.numEpisodes} x ${content.episodeRuntime} min`}
                </Pill>
            );
        } else if (content.runtime) {
            durationFragment = (
                <Pill>
                    <img src="/img/icons/time.svg" className={styles.iconTime} />
                    {`${content.runtime} min`}
                </Pill>
            );
        }

        const poster =
            posterImage ||
            "https://vignette.wikia.nocookie.net/citrus/images/6/60/No_Image_Available.png/revision/latest?cb=20170129011325";
        return (
            <>
                <div className={styles.dialogBody}>
                    <img src={poster} className={styles.poster} />
                    <div className={styles.info}>
                        <div className={styles.title}>{title}</div>
                        <div className={styles.contentInfo}>
                            <Pill>{year}</Pill>
                            {status && <Pill>{status}</Pill>}
                            {durationFragment}
                            {languageSvg && (
                                <img
                                    className={styles.languageFlag}
                                    src={`data:image/svg+xml;utf8,${encodeURIComponent(languageSvg)}`}
                                />
                            )}

                            <DotSeparator />
                            <StatusIcon content={content} className={styles.statusIcon} />
                        </div>

                        <div>
                            {genres?.map(g => (
                                <Pill key={g}>{g}</Pill>
                            ))}
                        </div>

                        <div className={styles.section}>
                            <div className={styles.headers}>Summary</div>
                            <div className={styles.sectionText}>{overview}</div>
                        </div>

                        <div className={styles.section}>
                            <div className={styles.headers}>Cast</div>
                            <div className={styles.sectionText}>{castPart}</div>
                        </div>
                    </div>
                    <div className={styles.rightSide}>
                        <div className={styles.gauge}>
                            <Rating
                                imdbRating={imdbRating}
                                imdbId={imdbId}
                                className={styles.rating}
                                ratingClassname={styles.score}
                            />
                            <CircularProgressbar
                                value={imdbRating}
                                maxValue={10}
                                strokeWidth={12}
                                styles={buildStyles({
                                    // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                                    // strokeLinecap: "butt",

                                    // Text size
                                    textSize: "16px",

                                    // How long animation takes to go from one percentage to another, in seconds
                                    pathTransitionDuration: 0.3,

                                    // Can specify path transition in more detail, or remove it entirely
                                    // pathTransition: 'none',

                                    // Colors
                                    pathColor: `#deb522`,
                                })}
                            />
                        </div>

                        {(streamingPlatforms || []).length > 0 && (
                            <div className={styles.streamProviders}>
                                <div className={styles.streamTitle}>Watch on</div>
                                <div>
                                    <StreamingPlatforms
                                        contentId={content.id}
                                        streamingPlatforms={streamingPlatforms || []}
                                    />
                                </div>
                            </div>
                        )}

                        <div className={styles.rightSideExpander}></div>

                        {lastRefresh && (
                            <div className={styles.lastRefresh}>
                                <div>Last update:</div>
                                <div>{getRelativeDays(lastRefresh)}</div>
                            </div>
                        )}
                    </div>
                </div>
            </>
        );
    }
}

FullContentInfo.propTypes = {
    content: PropTypes.object.isRequired,
};

export default FullContentInfo;
