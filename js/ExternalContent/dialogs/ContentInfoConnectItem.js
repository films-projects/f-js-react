import React from "react";
import PropTypes from "prop-types";

import styles from "./ContentInfoConnectItem.scss";

const ContentInfoConnectItem = ({ posterImage, overview, releaseDate, title, contentHasBeenAdded, buttons }) => {
    return (
        <div className={styles.item}>
            <img className={styles.poster} src={posterImage} />
            <div className={styles.info}>
                <div>{title}</div>
                <div className={styles.releaseDate}>{releaseDate}</div>
                <div className={styles.overview}>{overview}</div>

                {!contentHasBeenAdded && buttons}
                {contentHasBeenAdded && <span className={styles.alreadyAdded}>This content is already added!</span>}
            </div>
        </div>
    );
};

ContentInfoConnectItem.propTypes = {
    title: PropTypes.string.isRequired,
    posterImage: PropTypes.string.isRequired,
    overview: PropTypes.string.isRequired,
    releaseDate: PropTypes.string.isRequired,
    contentHasBeenAdded: PropTypes.bool,
    buttons: PropTypes.any,
};

export default ContentInfoConnectItem;
