import React from "react";

import search from "../search/reducers/searchReducer";
import watchOptions from "../Common/WatchOptions/reducer";
import searchFavorites from "../search/reducers/searchFavoritesReducer";

import SuggestFilmDialog from "../Content/dialogs/SuggestFilmDialog";
import AutoAddContentDialog from "../Content/dialogs/AutoAddContentDialog";
import AddFilmDialog from "../Content/dialogs/AddContentDialog";
import ShowContentInfoDialog from "../ExternalContent/dialogs/ShowContentInfoDialog";
import DeleteFilmDialog from "../Content/dialogs/DeleteContentDialog";
import ModifyFilmDialog from "../Content/dialogs/ModifyContentDialog";
import ConnectContentInfoDialog from "../ExternalContent/dialogs/ConnectContentInfoDialog";

import Page from "../Common/Page";
import SearchPage from "../search/SearchPage";

document.addEventListener("DOMContentLoaded", () => {
    new Page({
        reducers: { search, watchOptions, searchFavorites },
        dialogs: [
            AddFilmDialog,
            ModifyFilmDialog,
            DeleteFilmDialog,
            ShowContentInfoDialog,
            ConnectContentInfoDialog,
            SuggestFilmDialog,
            AutoAddContentDialog,
        ],
        page: (
            <div id="react-root" className="container">
                <SearchPage />
            </div>
        ),
    });
});
