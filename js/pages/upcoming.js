import React from "react";

import upcomingContent from "../UpcomingContent/reducers/upcomingContentReducer";

import Page from "../Common/Page";
import AutoAddContentDialog from "../Content/dialogs/AutoAddContentDialog";
import AddContentDialog from "../Content/dialogs/AddContentDialog";
import SuggestFilmDialog from "../Content/dialogs/SuggestFilmDialog";
import UpcomingContentPage from "../UpcomingContent/UpcomingContentPage";

document.addEventListener("DOMContentLoaded", () => {
    new Page({
        reducers: { upcomingContent },
        dialogs: [SuggestFilmDialog, AddContentDialog, AutoAddContentDialog],
        page: (
            <div id="react-root" className="container">
                <UpcomingContentPage />
            </div>
        ),
    });
});
