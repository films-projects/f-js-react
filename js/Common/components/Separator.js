import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./Separator.scss";

export const PipeSeparator = ({ className }) => {
    return <span className={cls(styles.separator, className)}>|</span>;
};

PipeSeparator.propTypes = {
    className: PropTypes.string,
};

export const DotSeparator = ({ className }) => {
    return <span className={cls(styles.separator, className)}>&middot;</span>;
};

DotSeparator.propTypes = {
    className: PropTypes.string,
};

export const SeparatedList = ({ listItems }) => {
    return listItems.map((item, index) => (
        <React.Fragment key={index}>
            {!!index && <PipeSeparator />}
            {item}
        </React.Fragment>
    ));
};

export const DotSeparatedList = ({ listItems }) => {
    return listItems.map((item, index) => (
        <React.Fragment key={index}>
            {!!index && <DotSeparator />}
            {item}
        </React.Fragment>
    ));
};
