import React from "react";
import PropTypes from "prop-types";

import styles from "./Toggle.scss";

const Toggle = ({ name, text, onChange, checked }) => (
    <div className={styles.checkboxWrapper}>
        <input id={name} type="checkbox" className={styles.switch} onChange={onChange} checked={checked} />
        <label htmlFor={name}>{text}</label>
    </div>
);

Toggle.propTypes = {
    text: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool.isRequired,
    onChange: PropTypes.func,
};

Toggle.defaultProps = {
    checked: false,
    onChange: () => {},
};

export default Toggle;
