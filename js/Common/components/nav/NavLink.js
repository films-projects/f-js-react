import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./Nav.scss";

const NavLink = ({ href, onClick, className, children }) => (
    <a href={href || "#"} onClick={onClick} className={cls(className, styles.link)}>
        {children}
    </a>
);

NavLink.propTypes = {
    children: PropTypes.any.isRequired,
    href: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
};

NavLink.defaultProps = {
    href: undefined,
    className: undefined,
    onClick: undefined,
};

export default NavLink;
