import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import cls from "classnames";
import onClickOutside from "react-onclickoutside";

import styles from "./SeriesQuickLink.scss";
import NavLink from "./NavLink";
import LoadingBar from "../LoadingBar";
import { loadUpcomingSeries } from "../../../Content/actions";
import { EpisodeNumber } from "../../../UpcomingContent/components/card/episodes/EpisodeDetails";
import { getRelativeDays } from "../../util/getRelativeTime";

class SeriesQuickLink extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            loading: true,
            data: [],
        };

        this.toggleOpen = this.toggleOpen.bind(this);
    }

    handleClickOutside() {
        this.setState({ open: false });
    }

    toggleOpen() {
        const isOpen = this.state.open;
        this.setState({ open: !this.state.open }, () => {
            if (!isOpen) {
                this.props.loadUpcomingSeries().then(data => this.setState({ data, loading: false }));
            }
        });
    }

    render() {
        let content;
        if (this.state.loading) {
            content = <LoadingBar className={styles.loading} />;
        } else if (!this.state.data.length) {
            content = (
                <>
                    <div>No series with upcoming episodes</div>
                    <a className={styles.viewAll} href="/upcoming">
                        See all upcoming content
                    </a>
                </>
            );
        } else {
            content = (
                <>
                    {this.state.data.map(s => (
                        <div key={s.title} className={styles.series}>
                            <a className={styles.title} href={`/search?title=${s.title}`}>
                                {s.title}
                            </a>
                            <span>
                                <EpisodeNumber
                                    episode={s.episode}
                                    className={cls(styles.episodeNumber, {
                                        [styles.lastEpisode]: s.episode.isFinale,
                                        [styles.firstEpisode]: s.episode.episode === 1,
                                    })}
                                />
                                <span className={styles.days}>{getRelativeDays(s.episode.airDate)}</span>
                            </span>
                        </div>
                    ))}

                    <a className={styles.viewAll} href="/upcoming">
                        See all upcoming content
                    </a>
                </>
            );
        }
        return (
            <div className={cls({ [styles.link]: true, [styles.open]: this.state.open })}>
                <NavLink onClick={this.toggleOpen}>
                    <img src="/img/icons/tv.svg" />
                </NavLink>
                <div className={styles.dropdown}>{content}</div>
            </div>
        );
    }
}

SeriesQuickLink.propTypes = {
    loadUpcomingSeries: PropTypes.func.isRequired,
};

export default connect(null, { loadUpcomingSeries })(onClickOutside(SeriesQuickLink));
