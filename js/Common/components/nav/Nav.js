import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import withOpenDialog from "../../HOC/WithOpenDialog";

import { PAGE_LOGOUT, PAGE_SEARCH, PAGE_UPCOMING } from "../../constants/pages";
import NavLink from "./NavLink";

import AutoAddContentDialog from "../../../Content/dialogs/AutoAddContentDialog";
import SuggestFilmDialog from "../../../Content/dialogs/SuggestFilmDialog";

import styles from "./Nav.scss";
import SeriesQuickLink from "./SeriesQuickLink";

class Nav extends React.Component {
    constructor(props) {
        super(props);
        this.state = { scrolled: false };

        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    handleScroll() {
        this.setState({ scrolled: window.scrollY > 40 });
    }

    render() {
        const { scrolled } = this.state;
        const { openDialog } = this.props;

        return (
            <div className={cls(styles.nav, { [styles.scrolled]: scrolled })}>
                <div className="container">
                    <div className={styles.links}>
                        <div className={styles.linksLeft}>
                            <NavLink href={PAGE_SEARCH}>Search</NavLink>
                            <NavLink onClick={() => openDialog(AutoAddContentDialog.dialogName)}>Add</NavLink>
                            <NavLink onClick={() => openDialog(SuggestFilmDialog.dialogName)}>Suggest Film</NavLink>
                            <NavLink href={PAGE_UPCOMING}>Upcoming</NavLink>
                        </div>
                        <div className={styles.linksCenter}></div>
                        <div className={styles.linksRight}>
                            <SeriesQuickLink />
                            <NavLink href={PAGE_LOGOUT} className={styles.logout}>
                                Logout
                            </NavLink>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Nav.propTypes = {
    openDialog: PropTypes.func.isRequired,
};

export default withOpenDialog(Nav);
