import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./Row.scss";

const FormRow = ({ className, children }) => <div className={cls(styles.row, className)}>{children}</div>;

FormRow.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
};

FormRow.defaultProps = { className: undefined };

export default FormRow;
