import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./SelectedOption.scss";

const SelectedOption = ({ option, multi, onDeselect }) =>
    !option ? null : multi ? (
        <div key={option.value} className={cls(styles.selected, styles.multi)}>
            <div className={styles.text}>{option.label}</div>
            <div
                className={styles.cross}
                onClick={e => {
                    e.stopPropagation();
                    onDeselect(option);
                }}
            >
                x
            </div>
        </div>
    ) : (
        <div className={styles.selected}>
            <div className={styles.texxt}>{option.label}</div>
        </div>
    );

SelectedOption.propTypes = {
    option: PropTypes.object.isRequired,
    multi: PropTypes.bool.isRequired,
    onDeselect: PropTypes.func.isRequired,
};

export default SelectedOption;
