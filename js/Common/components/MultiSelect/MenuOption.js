import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./MenuOption.scss";

const MenuOption = ({ option, onClick, selected }) => (
    <div
        className={cls(styles.option, option.className, {
            [styles.disabled]: option.disabled,
            [styles.selected]: selected,
        })}
        onClick={option.disabled || !onClick ? undefined : () => onClick(option)}
        key={option.value}
    >
        {option.label}
    </div>
);

MenuOption.propTypes = {
    option: PropTypes.object.isRequired,
    onClick: PropTypes.func,
    selected: PropTypes.bool,
};

MenuOption.defaultProps = {
    onClick: undefined,
    selected: undefined,
};

export default MenuOption;
