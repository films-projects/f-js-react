import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./SearchField.scss";

const SearchField = ({ searchable, onChange, value, placeholder }) => (
    <div
        contentEditable={searchable}
        className={cls(styles.input, { searchable })}
        placeholder={placeholder}
        onInput={e => onChange(e.target.innerHTML)}
        dangerouslySetInnerHTML={{ __html: value }}
    />
);

SearchField.propTypes = {
    placeholder: PropTypes.string,
    value: PropTypes.string,

    onChange: PropTypes.func.isRequired,
    searchable: PropTypes.bool.isRequired,
};

SearchField.defaultProps = {
    placeholder: undefined,
    value: undefined,
};

SearchField.propTypes = { placeholder: PropTypes.string };

SearchField.defaultProps = { placeholder: undefined };

export default SearchField;
