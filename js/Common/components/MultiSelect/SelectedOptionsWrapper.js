import React from "react";
import PropTypes from "prop-types";
import SearchField from "./SearchField";
import SelectedOption from "./SelectedOption";

import styles from "./MultiSelect.scss";

const SelectedOptionsWrapper = ({
    multi,
    onDeselect,
    searchable,
    placeholder,
    filter,
    options,
    onClick,
    doSearch,
    showPlaceholderWhenSelected,
}) => (
    <div className={styles.child} onClick={onClick}>
        {options.map(option => (
            <SelectedOption key={option.value} {...{ option, multi, onDeselect }} />
        ))}
        <SearchField
            {...{ placeholder, searchable }}
            placeholder={
                options.length === 0 || showPlaceholderWhenSelected
                    ? placeholder || (searchable ? "Type to search..." : "Select...")
                    : undefined
            }
            value={filter}
            onChange={doSearch}
        />
    </div>
);

SelectedOptionsWrapper.propTypes = {
    placeholder: PropTypes.string,
    filter: PropTypes.string,
    onClick: PropTypes.func,

    multi: PropTypes.bool.isRequired,
    onDeselect: PropTypes.func.isRequired,
    searchable: PropTypes.bool.isRequired,
    options: PropTypes.array.isRequired,
    doSearch: PropTypes.func.isRequired,
    showPlaceholderWhenSelected: PropTypes.bool.isRequired,
};

SelectedOptionsWrapper.defaultProps = {
    placeholder: undefined,
    filter: undefined,
    onClick: undefined,
};

export default SelectedOptionsWrapper;
