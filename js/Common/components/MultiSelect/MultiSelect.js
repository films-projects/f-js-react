import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";
import onClickOutside from "react-onclickoutside";

import SelectedOption from "./SelectedOption";
import MenuOption from "./MenuOption";
import ClearButton from "./ClearButton";
import SelectedOptionsWrapper from "./SelectedOptionsWrapper";

import styles from "./MultiSelect.scss";

//TODO backspace removes multi item
//TODO caret in contenteditable and enable searchable
//TODO arrow keys selecting
//TODO clearable option
class MultiSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            filter: undefined,
        };
        //selection
        this.sanitizeProps = this.sanitizeProps.bind(this);

        this.selectOption = this.selectOption.bind(this);
        this.deselectOption = this.deselectOption.bind(this);
        this.clearSelected = this.clearSelected.bind(this);

        this.mapSelectedOptions = this.mapSelectedOptions.bind(this);
        this.applyFilter = this.applyFilter.bind(this);
        this.getMenuOptions = this.getMenuOptions.bind(this);
        this.getSelectedOptions = this.getSelectedOptions.bind(this);
        this.doSearch = this.doSearch.bind(this);
        this.emitChange = this.emitChange.bind(this);
        this.getOptionList = this.getOptionList.bind(this);
    }

    handleClickOutside() {
        this.setState({ open: false });
    }

    clearSelected() {
        this.emitChange(this.props.multi ? [] : this.props.simpleValue ? undefined : {});
    }

    getSelectedOptions() {
        return this._options.filter(option => this._selectedValues.includes(option.value));
    }

    deselectOption(option) {
        const newValues = this._value.filter(val => val.value !== option.value);
        this.emitChange(this.props.simpleValue ? newValues.map(val => val.value) : newValues);
    }

    isSelected(option) {
        return this._selectedValues.includes(option.value);
    }

    doSearch(text) {
        return this.props.onSearch ? this.props.onSearch(text) : this.setState({ filter: text });
    }

    applyFilter() {
        return this.state.filter && !this.props.onSearch
            ? this._options.filter(
                  option => option.divider || option.label.toLowerCase().includes(this.state.filter.toLowerCase()),
              )
            : this._options;
    }

    mapSelectedOptions() {
        return this._value.map(option => (
            <SelectedOption
                key={option.value}
                option={option}
                multi={this.props.multi}
                onDeselect={this.deselectOption}
            />
        ));
    }

    /**
     * Convert input props into usable values
     * _value: array of selected option objects
     * _selectedValues: array of values for the objects in _value
     * _options: array of option objects
     */
    sanitizeProps() {
        const { value, options, multi } = this.props;

        if (options && !Array.isArray(options)) {
            throw new TypeError("MultiSelect requires array as options");
        }

        this._options = options || [];
        this._value = [];
        this._selectedValues = [];

        if (!value) {
            return;
        }

        if (multi) {
            if (!Array.isArray(value)) {
                throw new TypeError("MultiSelect requires array as value when allowing multiple options");
            } else {
                value.forEach(val =>
                    typeof val === "object"
                        ? this._value.push(this._options.find(option => option.value === val.value))
                        : this._value.push(this._options.find(option => option.value === val)),
                );
            }
            this._selectedValues = this._value.map(val => val.value);
        } else {
            const val =
                typeof value === "object"
                    ? this._options.find(option => option.value === value.value)
                    : this._options.find(option => option.value === value);
            if (val) {
                this._value.push(val);
                this._selectedValues.push(val.value);
            }
        }
    }

    emitChange(newValue) {
        if (this.props.onChange) {
            this.props.onChange(newValue);
        }
        let hasAvailableOptions = true;
        if (this.props.multi) {
            const newlySelectedValued = this.props.simpleValue ? newValue : newValue.map(val => val.value);
            hasAvailableOptions =
                this._options.filter(option => !newlySelectedValued.includes(option.value)).length > 0;
        }
        if (
            !this.props.multi ||
            this.props.multiCloseOnChange ||
            (this.props.multiCloseOnEmpty && !hasAvailableOptions)
        ) {
            this.setState({ open: false });
        }
        if (this.props.clearSearchOnChange) {
            this.setState({ filter: undefined });
        }
    }

    selectOption(option) {
        let newValue;
        if (this.props.multi) {
            newValue = this._value.concat(option);
            if (this.props.simpleValue) {
                newValue = newValue.map(option => option.value);
            }
        } else {
            newValue = this.props.simpleValue ? option.value : option;
        }

        this.emitChange(newValue);
    }

    getMenuOptions() {
        let options = this._options;
        if (this.props.multi && !this.props.multiShowSelected) {
            options = options.filter(option => !this._selectedValues.includes(option.value));
        }
        return options
            .filter(option => option.label !== undefined)
            .map(option => (
                <MenuOption
                    key={option.value}
                    selected={this.isSelected(option)}
                    option={option}
                    onClick={this.isSelected(option) ? this.deselectOption : this.selectOption}
                />
            ));
    }

    getOptionList() {
        const { emptyLabel, maxPicked, maxPickedMessage } = this.props;
        const menuOptions = this.getMenuOptions();
        if (maxPicked >= 0) {
            if (this._value.length >= maxPicked) {
                return <MenuOption option={{ label: maxPickedMessage, className: "empty" }} />;
            }
        }
        return menuOptions.length > 0 ? menuOptions : <MenuOption option={{ label: emptyLabel, className: "empty" }} />;
    }

    render() {
        this.sanitizeProps();
        const { open, filter } = this.state;
        const { clearable, disabled, className, placeholder, multi, showPlaceholderWhenSelected } = this.props;

        return (
            <div className={cls(styles["multi-select"], className, { [styles.open]: open, clearable, multi })}>
                <SelectedOptionsWrapper
                    {...{ multi, placeholder, filter, showPlaceholderWhenSelected }}
                    searchable={false}
                    onClick={disabled ? undefined : () => this.setState({ open: true })}
                    onDeselect={this.deselectOption}
                    doSearch={this.doSearch}
                    options={this._value}
                />
                {clearable ? <ClearButton onClick={this.clearSelected} /> : undefined}

                <div className={styles["multi-select-menu"]}>{this.getOptionList()}</div>
            </div>
        );
    }
}

MultiSelect.propTypes = {
    options: PropTypes.array.isRequired, //all pick options
    onSearch: PropTypes.func,
    /**
     * Callback function which received the current value
     */
    onChange: PropTypes.func,
    /**
     * Message to show in option list if no options are given
     */
    emptyLabel: PropTypes.string,
    className: PropTypes.string,
    placeholder: PropTypes.string,
    /**
     * The selected options
     */
    value: PropTypes.any, //TODO string or array or object
    /**
     * Whether or not the passed and returned values are just the value, or the whole object with value, label etc
     */
    simpleValue: PropTypes.bool,
    /**
     * If selecting multiple options, whether or not to keep the selected options in the option list
     */
    multiShowSelected: PropTypes.bool,
    /**
     * If true, close the option list after (de)selecting an option
     */
    multiCloseOnChange: PropTypes.bool,
    /**
     * If true, close the option list if there are no more options
     */
    multiCloseOnEmpty: PropTypes.bool,
    showPlaceholderWhenSelected: PropTypes.bool,
    clearSearchOnChange: PropTypes.bool,
    searchable: PropTypes.bool,
    clearable: PropTypes.bool,
    disabled: PropTypes.bool,
    multi: PropTypes.bool,
    /**
     * The max allowed number of options to be picked
     */
    maxPicked: PropTypes.number,
    /**
     * Message to be shown in the option list if the max number is reached
     */
    maxPickedMessage: PropTypes.string,
};

MultiSelect.defaultProps = {
    emptyLabel: "No results",
    multi: false,
    simpleValue: false,
    multiShowSelected: false,
    showPlaceholderWhenSelected: false,
    clearSearchOnChange: true,
    multiCloseOnChange: true,
    multiCloseOnEmpty: true,
    searchable: true,
    clearable: true,
    disabled: false,
    onChange: undefined,
    onSearch: undefined,
    value: undefined,
    placeholder: undefined,
    className: undefined,
    maxPicked: -1,
    maxPickedMessage: undefined,
};

export default onClickOutside(MultiSelect);
