import React from "react";
import PropTypes from "prop-types";

import styles from "./ClearButton.scss";

const ClearButton = ({ onClick }) => (
    <div onClick={onClick} className={styles.button}>
        x
    </div>
);

ClearButton.propTypes = { onClick: PropTypes.func.isRequired };

export default ClearButton;
