import React from "react";
import PropTypes from "prop-types";

const DialogContainerComponent = ({ dialog }) => <div>{dialog}</div>;

DialogContainerComponent.propTypes = { dialog: PropTypes.any };

DialogContainerComponent.defaultProps = { dialog: undefined };

export default DialogContainerComponent;
