import React from "react";
import PropTypes from "prop-types";

import Notification from "./Notification";

import styles from "./NotificationContainer.scss";

const NotificationContainerComponent = ({ notifications, removeNotification }) => (
    <div className={styles.container}>
        {notifications.map(notif => (
            <Notification key={notif.timestamp} {...notif} {...{ removeNotification }} />
        ))}
    </div>
);

NotificationContainerComponent.propTypes = {
    notifications: PropTypes.array.isRequired,
    removeNotification: PropTypes.func.isRequired,
};

export default NotificationContainerComponent;
