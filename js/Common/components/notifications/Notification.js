import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import { NOTIFICATION_HIDE_DURATION, NOTIFICATION_SHOW_DURATION } from "../../constants/notificationConstants";

import styles from "./Notification.scss";

class Notification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.hide = this.hide.bind(this);
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        setTimeout(this.hide, NOTIFICATION_SHOW_DURATION);
    }

    hide() {
        this.setState({ hiding: true });
        setTimeout(this.remove, NOTIFICATION_HIDE_DURATION);
    }

    remove() {
        this.props.removeNotification(this.props.timestamp);
    }

    render() {
        const clazz = cls(styles.notification, {
            [styles.hide]: this.state.hiding,
            [styles[this.props.type]]: this.props.type !== undefined,
        });
        return (
            <div className={clazz}>
                <div className={styles.icon} />
                <div>{this.props.message}</div>
            </div>
        );
    }
}

Notification.propTypes = {
    type: PropTypes.string.isRequired,
    message: PropTypes.any.isRequired,
    removeNotification: PropTypes.func.isRequired,
    timestamp: PropTypes.number.isRequired,
};

export default Notification;
