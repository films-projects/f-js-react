import React from "react";
import cls from "classnames";
import PropTypes from "prop-types";

import styles from "./LoadingBar.scss";

const LoadingBar = ({ className }) => (
    <div className={cls(styles.progressbarcontainer, className)}>
        <div className={styles.progressbar}>
            <div className={styles.progressbarvalue}></div>
        </div>
    </div>
);

LoadingBar.propTypes = {
    className: PropTypes.string,
};

export default LoadingBar;
