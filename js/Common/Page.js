import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";

import dialogReducer from "./reducers/dialogReducer";
import notifications from "./reducers/notificationReducer";

import NotificationContainer from "./containers/NotificationContainerContainer";
import DialogContainer from "./containers/DialogContainerContainer";
import Nav from "./components/nav/Nav";

import "../../scss/main.scss";

export default class {
    constructor(props = {}) {
        //config
        const { dialogs = [], reducers = {}, page } = props;
        const dialog = dialogReducer(dialogs);
        this.reducers = Object.assign({ dialog, notifications }, reducers || {});
        this.store = configureStore({
            reducer: this.reducers,
        });

        // render different parts of the page
        if (document.getElementById("navigation")) {
            const container = createRoot(document.getElementById("navigation"));
            container.render(
                <Provider store={this.store}>
                    <Nav />
                </Provider>,
            );
        }

        if (document.getElementById("notifications-container")) {
            const container = createRoot(document.getElementById("notifications-container"));
            container.render(
                <Provider store={this.store}>
                    <NotificationContainer />
                </Provider>,
            );
        }

        if (document.getElementById("dialog-container")) {
            const container = createRoot(document.getElementById("dialog-container"));
            container.render(
                <Provider store={this.store}>
                    <DialogContainer />
                </Provider>,
            );
        }

        const app = createRoot(document.getElementById("app"));
        app.render(<Provider store={this.store}>{page}</Provider>);
    }

    getStore() {
        return this.store;
    }
}
