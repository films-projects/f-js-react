import { A_ADD_NOTIFICATION, A_REMOVE_NOTIFICATION } from "../constants/dialogActionTypes";
import moment from "moment";

export default (state = [], { type, content }) => {
    switch (type) {
        case A_ADD_NOTIFICATION:
            return [...state, { ...content, timestamp: moment().valueOf() }];
        case A_REMOVE_NOTIFICATION:
            return state.filter(notification => notification.timestamp !== content);
        default:
            return state;
    }
};
