import { A_OPEN_DIALOG, A_CLOSE_DIALOGS } from "../constants/dialogActionTypes";
import React from "react";

export default dialogs => {
    const map = {};
    dialogs.forEach(dialog => (map[dialog.dialogName] = dialog));

    return (store = null, { type, content = {} }) => {
        switch (type) {
            case A_CLOSE_DIALOGS:
                return null;
            case A_OPEN_DIALOG: {
                const { dialogName, props } = content;
                const dialog = map[dialogName];
                if (!dialog) {
                    throw "Unknown dialog '" + dialogName + "'";
                }
                return React.createElement(dialog, props);
            }
            default:
                return store;
        }
    };
};
