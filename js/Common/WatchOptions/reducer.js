import { A_LOAD_WATCH_OPTIONS } from "./actions";

export default (state = [], { type, options }) => {
    switch (type) {
        case A_LOAD_WATCH_OPTIONS:
            return options;
        default:
            return state;
    }
};
