import { getJson } from "../util/defaultRequest";
import { LOAD_WATCH_OPTIONS } from "./endpoints";

export const getWatchOptions = () => getJson(LOAD_WATCH_OPTIONS);
