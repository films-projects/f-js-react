import { addNotificationFromError } from "../actions/notificationActions";
import { getWatchOptions } from "./requests";

export const A_LOAD_WATCH_OPTIONS = "A_LOAD_WATCH_OPTIONS";

export const loadWatchOptions = options => ({
    type: A_LOAD_WATCH_OPTIONS,
    options,
});

export const retrieveWatchOptions = () => dispatch =>
    getWatchOptions()
        .then(options => {
            dispatch(loadWatchOptions(options));
        })
        .catch(e => dispatch(addNotificationFromError(e)));
