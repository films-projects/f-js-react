import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./DialogButton.scss";

const DialogButton = ({ type, onClick, value, disabled, className }) => (
    <input
        type="button"
        className={cls(styles.button, className, styles[type])}
        onClick={onClick}
        value={value}
        disabled={disabled}
    />
);

DialogButton.propTypes = {
    type: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    className: PropTypes.string,
};

DialogButton.defaultProps = {
    disabled: false,
};

export default DialogButton;
