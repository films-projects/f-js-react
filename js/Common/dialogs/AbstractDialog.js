import React from "react";
import PropTypes from "prop-types";
import Modal from "react-modal";
import { connect } from "react-redux";
import cls from "classnames";

import { closeDialogs } from "../actions/dialogActions";

import DialogButtons from "./DialogButtons";
import { BUTTON_TYPE_GENERIC } from "../constants/buttonTypeConstants";

import styles from "./AbstractDialog.scss";

//TODO allow passing props
//TODO return null earlier in the data flow, so empty props arent passed to the dialogs
const Dialog = ({
    className,
    defaultClose,
    closeAction,
    title,
    children,
    leftAction,
    leftText,
    leftButtonType,
    leftDisabled,
    rightAction,
    rightText,
    rightButtonType,
    rightDisabled,
    centerAction,
    centerText,
    centerButtonType,
    centerDisabled,
}) => {
    const close = closeAction || defaultClose;
    const clazz = cls(styles.dialog, { [className]: className });

    return (
        <Modal className={clazz} isOpen={true} onRequestClose={close} overlayClassName={styles.overlay}>
            {title && <div className={styles.header}>{title}</div>}
            <div className={styles.body}>{children}</div>
            <DialogButtons
                leftAction={leftAction || close}
                {...{
                    leftText,
                    leftButtonType,
                    leftDisabled,
                    rightText,
                    rightAction,
                    rightButtonType,
                    rightDisabled,
                    centerAction,
                    centerText,
                    centerDisabled,
                    centerButtonType,
                }}
            />
        </Modal>
    );
};

Modal.setAppElement("#app");

Dialog.propTypes = {
    closeAction: PropTypes.func,
    className: PropTypes.string,

    title: PropTypes.string.isRequired,
    children: PropTypes.any.isRequired,
    defaultClose: PropTypes.func.isRequired,
    //passed to buttons
    leftText: PropTypes.string,
    leftAction: PropTypes.func,
    leftButtonType: PropTypes.string,
    leftDisabled: PropTypes.any,
    rightText: PropTypes.string,
    rightAction: PropTypes.func,
    rightButtonType: PropTypes.string,
    rightDisabled: PropTypes.any,
    centerText: PropTypes.string,
    centerAction: PropTypes.func,
    centerButtonType: PropTypes.string,
    centerDisabled: PropTypes.any,
};

Dialog.defaultProps = {
    closeAction: undefined,
    className: undefined,
    //passed to buttons
    leftText: "Cancel",
    rightText: "Submit",
    leftButtonType: BUTTON_TYPE_GENERIC,
    rightButtonType: BUTTON_TYPE_GENERIC,
    leftDisabled: false,
    rightDisabled: false,
    leftAction: undefined,
    rightAction: undefined,
};

const mapDispatchToProps = dispatch => ({ defaultClose: () => dispatch(closeDialogs()) });

const mapStateToProps = state => ({
    user: state.user,
    content: state.content,
});

export default connect(mapStateToProps, mapDispatchToProps)(Dialog);
