import React from "react";
import PropTypes from "prop-types";

import styles from "./DialogButtons.scss";
import DialogButton from "./DialogButton";

const DialogButtons = ({
    leftText,
    leftAction,
    leftButtonType,
    leftDisabled,
    rightText,
    rightAction,
    rightButtonType,
    rightDisabled,
    centerAction,
    centerButtonType,
    centerText,
    centerDisabled,
}) => (
    <div className={styles.buttons}>
        <DialogButton onClick={leftAction} type={leftButtonType} value={leftText} disabled={leftDisabled} />
        {centerAction && (
            <DialogButton onClick={centerAction} type={centerButtonType} value={centerText} disabled={centerDisabled} />
        )}
        {rightAction && (
            <DialogButton onClick={rightAction} type={rightButtonType} value={rightText} disabled={rightDisabled} />
        )}
    </div>
);

DialogButtons.propTypes = {
    leftDisabled: PropTypes.any.isRequired,
    leftAction: PropTypes.func.isRequired,
    leftText: PropTypes.string.isRequired,
    leftButtonType: PropTypes.string.isRequired,
    rightDisabled: PropTypes.any,
    rightAction: PropTypes.func,
    rightText: PropTypes.string,
    rightButtonType: PropTypes.string,
    centerDisabled: PropTypes.any,
    centerAction: PropTypes.func,
    centerText: PropTypes.string,
    centerButtonType: PropTypes.string,
};

export default DialogButtons;
