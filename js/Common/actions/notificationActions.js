import { A_ADD_NOTIFICATION, A_REMOVE_NOTIFICATION } from "../constants/dialogActionTypes";
import { NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_SUCCESS } from "../constants/notificationConstants";

export const addNotification = (message, type) => ({
    type: A_ADD_NOTIFICATION,
    content: { message, type },
});

export const addSuccessNotification = message => addNotification(message, NOTIFICATION_TYPE_SUCCESS);

export const addNotificationFromError = error =>
    addNotification(error.message || "An error occurred", NOTIFICATION_TYPE_ERROR);

export const removeNotification = id => ({
    type: A_REMOVE_NOTIFICATION,
    content: id,
});
