import { A_CLOSE_DIALOGS, A_OPEN_DIALOG } from "../constants/dialogActionTypes";

export const openDialog = (type, params) => ({
    type: A_OPEN_DIALOG,
    content: {
        dialogName: type,
        props: params,
    },
});

// TODO remove and use the dispatched action
export const closeDialogs = () => ({ type: A_CLOSE_DIALOGS });
export const closeAllDialogs = () => dispatch => dispatch({ type: A_CLOSE_DIALOGS });
