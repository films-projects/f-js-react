export const BUTTON_TYPE_SUCCESS = "success";
export const BUTTON_TYPE_DANGER = "error";
export const BUTTON_TYPE_WARN = "warn";
export const BUTTON_TYPE_GENERIC = "generic";
