export const NOTIFICATION_SHOW_DURATION = 3000; //3 sec
export const NOTIFICATION_HIDE_DURATION = 1000; //1 sec

export const NOTIFICATION_TYPE_ERROR = "error";
export const NOTIFICATION_TYPE_INFORM = "inform";
export const NOTIFICATION_TYPE_SUCCESS = "success";
