export const statusMap = {
    UPCOMING: "Upcoming",
    ONGOING: "Ongoing",
    CANCELLED: "Cancelled",
    ENDED: "Ended",
};
