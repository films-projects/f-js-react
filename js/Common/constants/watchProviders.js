export const NETFLIX = "NETFLIX";
export const PRIME_VIDEO = "AMAZON_PRIME";
export const APPLE_TV = "APPLE_TV";
export const HBO_MAX = "HBO_MAX";
export const SKY_SHOWTIME = "SKY_SHOWTIME";

export const watchOptionMap = {
    [NETFLIX]: "netflix.svg",
    [PRIME_VIDEO]: "prime.svg",
    [APPLE_TV]: "apple.svg",
    [HBO_MAX]: "hbomax.svg",
    [SKY_SHOWTIME]: "showtime.svg",
};

export const friendlyNames = {
    [NETFLIX]: "Netflix",
    [PRIME_VIDEO]: "Prime Video",
    [APPLE_TV]: "Apple TV+",
    [HBO_MAX]: "HBO Max",
    [SKY_SHOWTIME]: "SkyShowtime",
};
