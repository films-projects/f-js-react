import React from "react";
import { connect } from "react-redux";

import { openDialog } from "../actions/dialogActions";

export default ComposedClass => {
    const Component = props => <ComposedClass {...props} />;
    Component.dialogName = ComposedClass.dialogName;
    return connect(undefined, { openDialog })(Component);
};
