import { connect } from "react-redux";
import { closeDialogs } from "../actions/dialogActions";

import DialogContainerComponent from "../components/DialogContainerComponent";

export default connect(({ dialog }) => ({ dialog }), { closeDialogs })(DialogContainerComponent);
