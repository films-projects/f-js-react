import { connect } from "react-redux";

import { removeNotification } from "../actions/notificationActions";

import NotificationContainerComponent from "../components/notifications/NotificationContainerComponent";

export default connect(({ notifications }) => ({ notifications }), { removeNotification })(
    NotificationContainerComponent,
);
