import fetch from "node-fetch";

const checkResponseStatus = res => {
    if (res.status === 403) {
        throw new Error("You are not allowed to perform this action");
    }
    if (res.status === 404) {
        throw new Error("The requested resource could not be found");
    }
    if (res.status >= 400) {
        throw new Error();
    }
    return res;
};

export const get = (url, options) => fetch(url, options).then(checkResponseStatus);

export const getJson = (url, options) => get(url, options).then(res => res.json());

export const post = (url, options = {}) => {
    const postOptions = Object.assign(options, { method: "POST" });
    return get(url, postOptions).then(checkResponseStatus);
};

export const postJson = (url, body) =>
    post(url, {
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" },
    });

export const patch = (url, patch) =>
    get(url, {
        method: "PATCH",
        body: JSON.stringify(patch),
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json-patch+json",
        },
    });
