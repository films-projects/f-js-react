import React from "react";

export const txt = (value = "", onChange, placeholder, name) => (
    <input type="text" {...{ value, onChange, placeholder, name }} />
);

export const selRTL = (value, cb, options, id, name) => (
    <select id={id} name={name} dir="rtl" onChange={cb} value={value}>
        {Object.keys(options).map(k => (
            <option key={k} value={k}>
                {options[k]}
            </option>
        ))}
    </select>
);
