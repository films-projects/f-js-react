import React from "react";

const EXTRA_YEARS = 2;

export const minYear = 1900;
export const maxYear = new Date().getFullYear() + EXTRA_YEARS;

export const getYears = () => {
    const years = [];
    for (let i = maxYear; i >= minYear; i--) {
        years.push(i);
    }
    return years;
};

export default getYears().map(i => (
    <option key={i} value={i}>
        {i}
    </option>
));
