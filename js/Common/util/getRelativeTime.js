import moment from "moment";

/**
 * Timestamp in format 'YYYY-MM-DD'
 */
export const getRelativeDays = timestamp => {
    const momentObj = moment(timestamp, "YYYY-MM-DD");
    const now = moment().startOf("day");
    const difference = moment.duration(now.diff(momentObj)).asDays();
    const days = Math.round(Math.abs(difference));
    const singularOrPlural = days === 1 ? "day" : "days";
    if (difference === 0) {
        return "Today";
    }
    return difference > 0 ? `${days} ${singularOrPlural} ago` : `in ${days} ${singularOrPlural}`;
};
