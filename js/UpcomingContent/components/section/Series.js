import React, { useState } from "react";
import PropTypes from "prop-types";
import Section from "./Section";
import SeriesCard from "../card/SeriesCard";

import containerStyles from "../card/CardContainer.scss";
import Toggle from "../../../Common/components/toggle/Toggle";
import CardContainer from "../card/CardContainer";

const Series = ({ seriesAndEpisodes, loading }) => {
    const [showScheduled, setShowScheduled] = useState(true);
    const [showUpcoming, setShowUpcoming] = useState(false);
    const [showUnscheduled, setShowUnscheduled] = useState(false);

    let filteredSeries = seriesAndEpisodes;
    if (!showUpcoming) {
        filteredSeries = filteredSeries.filter(entry => entry.series.status !== "Upcoming");
    }
    if (!showUnscheduled) {
        filteredSeries = filteredSeries.filter(
            entry => entry.series.status === "Upcoming" || entry.nextEpisode?.airDate,
        );
    }
    if (!showScheduled) {
        filteredSeries = filteredSeries.filter(entry => entry.nextEpisode?.airDate == null);
    }

    return (
        <Section
            title="Series"
            loading={loading}
            headerText={`${seriesAndEpisodes.length} series currently watching, or interested`}
        >
            <div className={containerStyles.toggleWrapper}>
                <div className={containerStyles.toggle}>
                    <Toggle
                        name="scheduled"
                        text="Scheduled"
                        checked={showScheduled}
                        onChange={() => setShowScheduled(!showScheduled)}
                    />
                </div>
                <div className={containerStyles.toggle}>
                    <Toggle
                        name="upcoming"
                        text="Upcoming"
                        checked={showUpcoming}
                        onChange={() => setShowUpcoming(!showUpcoming)}
                    />
                </div>
                <div className={containerStyles.toggle}>
                    <Toggle
                        name="notscheduled"
                        text="Not scheduled"
                        checked={showUnscheduled}
                        onChange={() => setShowUnscheduled(!showUnscheduled)}
                    />
                </div>
            </div>
            {filteredSeries.length === 0 && seriesAndEpisodes.length > 0 && (
                <div>There are no upcoming series for the selected filters</div>
            )}
            {filteredSeries.length > 0 && (
                <CardContainer className={containerStyles.seriesContainer}>
                    {filteredSeries.map(s => (
                        <SeriesCard
                            key={s.series.id}
                            series={s.series}
                            lastEpisode={s.lastEpisode}
                            nextEpisode={s.nextEpisode}
                        />
                    ))}
                </CardContainer>
            )}
        </Section>
    );
};

Series.propTypes = {
    seriesAndEpisodes: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
};

export default Series;
