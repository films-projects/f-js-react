import React from "react";
import PropTypes from "prop-types";
import Section from "./Section";
import MovieCard from "../card/MovieCard";

import containerStyles from "../card/CardContainer.scss";
import CardContainer from "../card/CardContainer";

const RecentMovies = ({ movies, loading }) => (
    <Section
        title="Recently released movies"
        loading={loading}
        headerText={`${movies.length} recently released, unseen movies`}
    >
        <CardContainer className={containerStyles.movieContainer}>
            {movies.map(m => (
                <MovieCard key={m.id} movie={m} />
            ))}
        </CardContainer>
    </Section>
);

RecentMovies.propTypes = {
    movies: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
};

export default RecentMovies;
