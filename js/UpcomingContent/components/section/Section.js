import React from "react";
import PropTypes from "prop-types";

import styles from "./Section.scss";
import LoadingBar from "../../../Common/components/LoadingBar";

const Section = ({ title, headerText, children, loading }) => {
    if (loading) {
        return (
            <div className={styles.section}>
                <div className={styles.header}>
                    <div className={styles.sectionTitle}>{title}</div>
                </div>
                <LoadingBar />
            </div>
        );
    }
    return (
        <div className={styles.section}>
            <div className={styles.header}>
                {headerText && <div className={styles.headerText}>{headerText}</div>}
                <div className={styles.sectionTitle}>{title}</div>
            </div>
            {children}
        </div>
    );
};

Section.propTypes = {
    title: PropTypes.string.isRequired,
    loading: PropTypes.bool.isRequired,
    headerText: PropTypes.string,
    children: PropTypes.array,
};

export default Section;
