import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import classnames from "classnames";

import styles from "./EpisodeDetails.scss";
import { getRelativeDays } from "../../../../Common/util/getRelativeTime";

export const EpisodeNumber = ({ episode, className }) => {
    let seasonAndEpisode = episode.episode + "";
    if (seasonAndEpisode.length < 2) {
        seasonAndEpisode = seasonAndEpisode.padStart(2, "0");
    }
    seasonAndEpisode = "E" + seasonAndEpisode;
    seasonAndEpisode = episode.season + seasonAndEpisode;
    if (seasonAndEpisode.length < 5) {
        seasonAndEpisode = seasonAndEpisode.padStart(5, "0");
    }
    seasonAndEpisode = "S" + seasonAndEpisode;
    return <span className={classnames(styles.button, className, styles.seasonAndEpisode)}>{seasonAndEpisode}</span>;
};

EpisodeNumber.propTypes = {
    episode: PropTypes.object.isRequired,
    className: PropTypes.string,
};

const EpisodeDetails = ({ episode, className }) => {
    if (!episode) {
        return <div className={classnames(styles.episode)}></div>;
    }

    const momentObj = moment(episode.airDate, "YYYY-MM-DD");
    const now = moment().startOf("day");
    const difference = moment.duration(now.diff(momentObj)).asDays();
    const days = Math.round(Math.abs(difference));

    return (
        <div className={classnames(styles.episode, className)}>
            <div className={styles.buttons}>
                <EpisodeNumber episode={episode} />
                {episode.isFinale && <span className={classnames(styles.button, styles.finaleButton)}>Finale</span>}
                {episode.episode === 1 && <span className={classnames(styles.button, styles.startButton)}>Start</span>}
            </div>
            <div className={styles.airDate}>
                {days <= 30 && (
                    <>
                        <div>{getRelativeDays(episode.airDate)}</div>
                        <div className={styles.exact}>{moment(episode.airDate, "YYYY-MM-DD").format("DD-MM-YYYY")}</div>
                    </>
                )}
                {days > 30 && <div>{moment(episode.airDate, "YYYY-MM-DD").format("DD-MM-YYYY")}</div>}
            </div>
        </div>
    );
};

EpisodeDetails.propTypes = {
    className: PropTypes.string,
    episode: PropTypes.object,
};

export default EpisodeDetails;
