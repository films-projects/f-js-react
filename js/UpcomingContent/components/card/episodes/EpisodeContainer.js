import React from "react";
import PropTypes from "prop-types";
import EpisodeDetails from "./EpisodeDetails";

import styles from "./EpisodeContainer.scss";

const EpisodeContainer = ({ lastEpisode, nextEpisode, episodeClass }) => (
    <div className={styles.container}>
        <EpisodeDetails episode={lastEpisode} className={episodeClass} />
        <EpisodeDetails episode={nextEpisode} className={episodeClass} />
    </div>
);

EpisodeContainer.propTypes = {
    episodeClass: PropTypes.string,
    lastEpisode: PropTypes.object,
    nextEpisode: PropTypes.object,
};

export default EpisodeContainer;
