import React from "react";
import PropTypes from "prop-types";
import moment from "moment";

import styles from "./MoviesCard.scss";

const MovieCard = ({ movie }) => (
    <div className={styles.card}>
        <img className={styles.poster} src={movie.posterUrl}></img>
        <div className={styles.cardInfo}>
            <div className={styles.cardTitle}>
                <a href={`/search?title=${movie.title}`} className={styles.title}>
                    {movie.title}
                </a>
            </div>
            <div className={styles.bottom}>
                <span className={styles.releaseDate}>
                    {moment(movie.releaseDate, "YYYY-MM-DD").format("DD-MM-YYYY")}
                </span>
                {movie.specialInterest && (
                    <img className={styles.specialInterest} src="/img/icons/anticipated-yes.svg" />
                )}
            </div>
        </div>
    </div>
);

MovieCard.propTypes = {
    movie: PropTypes.object.isRequired,
};

export default MovieCard;
