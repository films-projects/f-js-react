import React from "react";
import PropTypes from "prop-types";

import styles from "./SeriesCard.scss";
import EpisodeContainer from "./episodes/EpisodeContainer";

const SeriesCard = ({ series, lastEpisode, nextEpisode }) => (
    <div className={styles.card}>
        <img className={styles.poster} src={series.posterUrl}></img>
        <div className={styles.cardInfo}>
            <div className={styles.cardTitle}>
                <a href={`/search?title=${series.title}`} className={styles.title}>
                    {series.title}
                </a>
                <span className={styles.titleRight}>{series.status}</span>
            </div>
            <EpisodeContainer lastEpisode={lastEpisode} nextEpisode={nextEpisode} />
        </div>
    </div>
);

SeriesCard.propTypes = {
    series: PropTypes.object.isRequired,
    lastEpisode: PropTypes.object,
    nextEpisode: PropTypes.object,
};

export default SeriesCard;
