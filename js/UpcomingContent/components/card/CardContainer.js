import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import styles from "./CardContainer.scss";
const CardContainer = ({ children, className }) => (
    <div className={classnames(styles.cardContainer, className)}>{children}</div>
);

CardContainer.propTypes = {
    children: PropTypes.array.isRequired,
    className: PropTypes.string,
};

export default CardContainer;
