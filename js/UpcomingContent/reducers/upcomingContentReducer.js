import { LOAD_UPCOMING_CONTENT } from "../actionTypes";

export default (state = { loading: true, series: [], upcomingMovies: [], recentMovies: [] }, { type, data }) => {
    switch (type) {
        case LOAD_UPCOMING_CONTENT:
            return data;
        default:
            return state;
    }
};
