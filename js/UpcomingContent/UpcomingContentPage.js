import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { doLoadContent } from "./actions";
import UpcomingMovies from "./components/section/UpcomingMovies";
import RecentMovies from "./components/section/RecentMovies";
import Series from "./components/section/Series";

class UpcomingContentPage extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.doLoadContent();
    }

    render() {
        const { loading, series, upcomingMovies, recentMovies } = this.props;
        return (
            <>
                <RecentMovies movies={recentMovies} loading={loading} />
                <UpcomingMovies movies={upcomingMovies} loading={loading} />
                <Series seriesAndEpisodes={series} loading={loading} />
            </>
        );
    }
}

UpcomingContentPage.propTypes = {
    doLoadContent: PropTypes.func.isRequired,
    series: PropTypes.array.isRequired,
    upcomingMovies: PropTypes.array.isRequired,
    recentMovies: PropTypes.array.isRequired,
    loading: PropTypes.bool,
};

UpcomingContentPage.defaultProps = {
    loading: false,
};

export default connect(
    ({ upcomingContent: { loading, series, upcomingMovies, recentMovies } }) => ({
        loading,
        upcomingMovies,
        recentMovies,
        series,
    }),
    { doLoadContent },
)(UpcomingContentPage);
