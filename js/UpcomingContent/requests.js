import { getJson } from "../Common/util/defaultRequest";
import { UPCOMING_CONTENT } from "./endpoints";

export const loadContent = () => getJson(UPCOMING_CONTENT);
