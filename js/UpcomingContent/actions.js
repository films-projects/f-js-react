import { loadContent } from "./requests";
import { addNotificationFromError } from "../Common/actions/notificationActions";
import { LOAD_UPCOMING_CONTENT } from "./actionTypes";

export const loadUpcomingContent = data => ({
    type: LOAD_UPCOMING_CONTENT,
    data: data,
});

export const doLoadContent = () => dispatch =>
    loadContent()
        .then(content => dispatch(loadUpcomingContent(content)))
        .catch(e => dispatch(addNotificationFromError(e)));
