import queryString from "query-string";

export const ADD_CONTENT = type => `/${type}/new`;
export const ADD_CONTENT_FROM_EXTERNAL = (type, externalId, interested) =>
    `/${type}/newFromExternal/${externalId}?interested=${interested}`;
export const UPDATE_CONTENT = (contentId, params) => `/content/${contentId}/update?${queryString.stringify(params)}`;
export const DELETE_CONTENT = id => `/content/${id}/delete`;
export const SUGGEST_FILM = params => `/film/suggest?${queryString.stringify(params)}`;
export const LOAD_UPCOMING_SERIES = "/series/upcoming";
