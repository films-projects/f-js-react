import { connect } from "react-redux";

import { setMyRating } from "../actions";
import StatusIcons from "../components/slide/bottom/StatusIcons";

export default connect(undefined, { setMyRating: setMyRating })(StatusIcons);
