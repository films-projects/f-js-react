import { connect } from "react-redux";

import { setAnticipated } from "../actions";
import AnticipatedToggle from "../components/slide/bottom/AnticipatedToggle";

export default connect(undefined, { setAnticipated: setAnticipated })(AnticipatedToggle);
