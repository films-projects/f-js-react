import { connect } from "react-redux";

import { setWatching } from "../actions";
import WatchingToggle from "../components/slide/bottom/WatchingToggle";

export default connect(undefined, { setWatching })(WatchingToggle);
