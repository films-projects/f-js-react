import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Dialog from "../../Common/dialogs/AbstractDialog";
import { BUTTON_TYPE_DANGER } from "../../Common/constants/buttonTypeConstants";

import { deleteContent } from "../actions";

import styles from "./DeleteContentDialog.scss";

const DeleteContentDialog = ({ content, deleteContent }) => (
    <Dialog
        className={styles.dialog}
        rightAction={() => deleteContent(content)}
        rightText="Delete"
        rightButtonType={BUTTON_TYPE_DANGER}
        title="Delete content"
    >
        <p>
            Are you sure you want to delete:
            <div className={styles.title}>{content.title}</div>
        </p>
    </Dialog>
);

DeleteContentDialog.dialogName = "DIALOG_DELETE_CONTENT";

DeleteContentDialog.propTypes = {
    content: PropTypes.object.isRequired,
    deleteContent: PropTypes.func.isRequired,
};

export default connect(undefined, { deleteContent })(DeleteContentDialog);
