import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import cls from "classnames";

import { BUTTON_TYPE_GENERIC, BUTTON_TYPE_SUCCESS } from "../../Common/constants/buttonTypeConstants";
import Row from "../../Common/components/form/Row";
import Dialog from "../../Common/dialogs/AbstractDialog";
import { txt } from "../../Common/util/createInput";

import { add } from "../actions";
import { doSearch } from "../../search/actions";
import { maxYear, minYear } from "../../Common/util/getAllowedYears";

import styles from "./AddContentDialog.scss";
import withOpenDialog from "../../Common/HOC/WithOpenDialog";
import AutoAddContentDialog from "./AutoAddContentDialog";

class AddContentDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = { year: new Date().getFullYear(), type: "film", rating: 0, anticipated: false, watching: false };
        this.addContent = this.addContent.bind(this);
    }

    addContent() {
        this.props.add(this.state.type, this.state).then(() => {
            if (this.props.params) {
                this.props.doSearch(this.props.params);
            }
        });
    }

    render() {
        const createChange = name => e => this.setState({ [name]: e.target.value });

        const { title, year, rating, type, watching, anticipated } = this.state;

        return (
            <Dialog
                className={styles["dialog-add-film"]}
                leftText="Search content"
                leftAction={() => this.props.openDialog(AutoAddContentDialog.dialogName)}
                rightAction={() => this.addContent()}
                rightText="Add"
                leftButtonType={BUTTON_TYPE_GENERIC}
                rightButtonType={BUTTON_TYPE_SUCCESS}
                title="Add content"
            >
                <Row className={styles.row}>
                    <label>Type</label>

                    <div className={styles.iconRow}>
                        <div
                            className={cls({ [styles.icon]: true, [styles.selected]: type === "film" })}
                            onClick={() => this.setState({ type: "film" })}
                        >
                            Film
                        </div>
                        <div
                            className={cls({ [styles.icon]: true, [styles.selected]: type === "series" })}
                            onClick={() => this.setState({ type: "series" })}
                        >
                            Series
                        </div>
                    </div>
                </Row>
                <Row className={styles.row}>
                    <label htmlFor="input-title">Title</label>
                    {txt(title, createChange("title"), "Title")}
                </Row>
                <Row className={styles.row}>
                    <label>Year</label>
                    <input
                        type="number"
                        onChange={e => {
                            const value = e.target.value;
                            if (value < minYear) {
                                this.setState({ year: minYear });
                            } else if (value > maxYear) {
                                this.setState({ year: maxYear });
                            } else {
                                this.setState({ year: value });
                            }
                        }}
                        value={year}
                        min={minYear}
                        max={maxYear}
                    />
                </Row>
                <Row className={styles.row}>
                    <label>My rating</label>
                    <div className={styles.iconRow}>
                        {[-1, 0, 1, 2, 3, 4, 5].map(val => {
                            return (
                                <div
                                    key={val}
                                    className={cls({
                                        [styles.icon]: true,
                                        [styles[`rating-${val}`]]: true,
                                        [styles.selected]: rating === val,
                                    })}
                                    onClick={() => this.setState({ rating: val })}
                                />
                            );
                        })}
                    </div>
                </Row>
                <Row className={styles.row}>
                    <label>Interested</label>
                    <div className={styles.iconRow}>
                        {[false, true].map(val => {
                            return (
                                <div
                                    key={val}
                                    className={cls({
                                        [styles.icon]: true,
                                        [styles[`interested-${val}`]]: true,
                                        [styles.selected]: anticipated === val,
                                    })}
                                    onClick={() => this.setState({ anticipated: val })}
                                />
                            );
                        })}
                    </div>
                </Row>
                {type === "series" && (
                    <Row className={styles.row}>
                        <label>Watching</label>
                        <div className={styles.iconRow}>
                            {[false, true].map(val => {
                                return (
                                    <div
                                        key={val}
                                        className={cls({
                                            [styles.icon]: true,
                                            [styles[`watching-${val}`]]: true,
                                            [styles.selected]: watching === val,
                                        })}
                                        onClick={() => this.setState({ watching: val })}
                                    />
                                );
                            })}
                        </div>
                    </Row>
                )}
            </Dialog>
        );
    }
}

AddContentDialog.dialogName = "DIALOG_ADD_FILM";

AddContentDialog.propTypes = {
    add: PropTypes.func.isRequired,
    doSearch: PropTypes.func.isRequired,
    openDialog: PropTypes.func.isRequired,
    params: PropTypes.object,
};

export default withOpenDialog(
    connect(
        ({ search }) => ({
            params: search?.params,
        }),
        { add, doSearch },
    )(AddContentDialog),
);
