import React from "react";
import debounce from "debounce";

import { BUTTON_TYPE_GENERIC, BUTTON_TYPE_SUCCESS } from "../../Common/constants/buttonTypeConstants";
import Dialog from "../../Common/dialogs/AbstractDialog";

import { suggestFilm } from "../requests";

import LoadingBar from "../../Common/components/LoadingBar";

import styles from "./SuggestFilmDialog.scss";
import MultiSelect from "../../Common/components/MultiSelect/MultiSelect";
import { watchOptionMap } from "../../Common/constants/watchProviders";
import FullContentInfo from "../../ExternalContent/dialogs/FullContentInfo";

class SuggestFilmDialog extends React.Component {
    constructor(props) {
        super(props);

        this.getSuggestion = debounce(this.getSuggestion.bind(this), 100);
        this.setSeenAndInterested = this.setSeenAndInterested.bind(this);
        this.state = {
            seen: "false",
            anticipated: "true",
            film: {},
            loading: true,
            minRating: "",
            watchOptions: [],
            seenDropdown: 1,
        };
    }

    componentDidMount() {
        this.getSuggestion();
    }

    getSuggestion() {
        const { seen, minRating, watchOptions, anticipated } = this.state;
        suggestFilm({ minRating, seen, anticipated, watchOptions }).then(film =>
            this.setState({ film, loading: false }),
        );
    }

    setSeenAndInterested(value) {
        if (value === "0") {
            this.setState({ seen: false, anticipated: undefined, seenDropdown: value }, this.getSuggestion);
        } else if (value === "1") {
            this.setState({ seen: false, anticipated: true, seenDropdown: value }, this.getSuggestion);
        } else {
            this.setState({ seen: true, anticipated: undefined, seenDropdown: value }, this.getSuggestion);
        }
    }

    render() {
        const { film, loading, minRating, watchOptions, seenDropdown } = this.state;

        let body;

        if (loading) {
            body = <LoadingBar />;
        } else if (!film.title) {
            body = <div className={styles.empty}>No film found!</div>;
        } else {
            body = (
                <div className={styles.suggestion}>
                    <FullContentInfo content={film} />
                </div>
            );
        }

        const op = Object.keys(watchOptionMap).map(provider => {
            return { value: provider, label: <img src={`/img/external/${watchOptionMap[provider]}`} /> };
        });

        return (
            <Dialog
                className={styles.dialog}
                rightAction={this.getSuggestion}
                rightText="Another!"
                leftButtonType={BUTTON_TYPE_GENERIC}
                rightButtonType={BUTTON_TYPE_SUCCESS}
                title="How about..."
            >
                <div className={styles.selectors}>
                    <input
                        type="number"
                        className={styles.minRating}
                        placeholder="Minimum rating"
                        min="0"
                        max="9.5"
                        step="0.5"
                        onChange={e => this.setState({ minRating: e.target.value }, this.getSuggestion)}
                        value={minRating}
                        name="minRating"
                    />

                    <select
                        value={seenDropdown}
                        className={styles.seen}
                        onChange={e => this.setSeenAndInterested(e.target.value)}
                    >
                        <option value={0}>Not seen</option>
                        <option value={1}>Interested</option>
                        <option value={2}>Already seen</option>
                    </select>

                    <MultiSelect
                        placeholder="Select watch options"
                        className={styles.multi}
                        onChange={watchOptions => this.setState({ watchOptions }, this.getSuggestion)}
                        multi={true}
                        options={op}
                        value={watchOptions}
                        simpleValue={true}
                    />
                </div>
                {body}
            </Dialog>
        );
    }
}

SuggestFilmDialog.dialogName = "DIALOG_SUGGEST_FILM";

export default SuggestFilmDialog;
