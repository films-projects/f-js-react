import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Row from "../../Common/components/form/Row";
import { txt } from "../../Common/util/createInput";
import Dialog from "../../Common/dialogs/AbstractDialog";

import { update } from "../actions";
import allowedYears, { getYears } from "../../Common/util/getAllowedYears";

import styles from "./ModifyContentDialog.scss";

class ModifyContentDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, props.content);
    }

    render() {
        const map = {};
        getYears().forEach(year => (map[year + ""] = year));
        const { title, year } = this.state;

        const { update } = this.props;
        const createChange = name => e => this.setState({ [name]: e.target.value });

        return (
            <Dialog
                className={styles.modify}
                rightAction={() => update(this.state)}
                rightText="Save"
                title="Edit Content"
            >
                <Row className={styles.row}>
                    <label>Title</label>
                    {txt(title, createChange("title"), "Title")}
                </Row>
                <Row className={styles.row}>
                    <label>Year</label>
                    <select dir="rtl" onChange={createChange("year")} value={year}>
                        {allowedYears}
                    </select>
                </Row>
            </Dialog>
        );
    }
}

ModifyContentDialog.propTypes = {
    content: PropTypes.object.isRequired,
    update: PropTypes.func.isRequired,
};

ModifyContentDialog.dialogName = "DIALOG_MODIFY_CONTENT";

export default connect(null, { update })(ModifyContentDialog);
