import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import debounce from "debounce";
import cls from "classnames";

import { BUTTON_TYPE_GENERIC } from "../../Common/constants/buttonTypeConstants";
import Row from "../../Common/components/form/Row";
import Dialog from "../../Common/dialogs/AbstractDialog";
import { txt } from "../../Common/util/createInput";
import { loadContentInfoSuggestionsByTitle } from "../../ExternalContent/actions/contentInfoActions";

import { addFromExternal } from "../actions";
import { doSearch } from "../../search/actions";

import LoadingBar from "../../Common/components/LoadingBar";
import ContentInfoConnectItem from "../../ExternalContent/dialogs/ContentInfoConnectItem";
import withOpenDialog from "../../Common/HOC/WithOpenDialog";
import AddContentDialog from "./AddContentDialog";

import styles from "./AutoAddContentDialog.scss";
import connectStyles from "../../ExternalContent/dialogs/ContentInfoConnectItem.scss";

class AutoAddContentDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = { type: "film", title: "", loading: false };
        this.addContent = this.addContent.bind(this);
        this.searchContent = debounce(this.searchContent.bind(this), 500);
    }

    searchContent() {
        const { type, title } = this.state;
        if (title) {
            this.setState({ loading: true });
            this.props
                .loadContentInfo(type.toUpperCase(), title)
                .then(suggestions => this.setState({ suggestions, loading: false }));
        }
    }

    addContent(externalId, interested) {
        this.props.add(this.state.type, externalId, interested).then(() => {
            if (this.props.params) {
                this.props.doSearch(this.props.params);
            }
        });
    }

    render() {
        const createChange = name => e => this.setState({ [name]: e.target.value }, this.searchContent);

        const { title, loading, suggestions } = this.state;
        const { openDialog } = this.props;

        return (
            <Dialog
                className={styles.addDialog}
                rightAction={() => openDialog(AddContentDialog.dialogName)}
                rightText="Add manually"
                leftButtonType={BUTTON_TYPE_GENERIC}
                rightButtonType={BUTTON_TYPE_GENERIC}
                title="Add content"
            >
                <Row className={styles.row}>
                    <label>Type</label>
                    <span onChange={createChange("type")}>
                        <label>
                            <input type="radio" value="film" name="type" defaultChecked /> Film
                        </label>
                        <label>
                            <input type="radio" value="series" name="type" /> Series
                        </label>
                    </span>
                </Row>
                <Row className={styles.row}>
                    <label htmlFor="input-title">Title</label>
                    {txt(title, createChange("title"))}
                </Row>

                <div className={styles.results}>
                    {loading ? (
                        <LoadingBar />
                    ) : (
                        (suggestions || []).map(suggestion => (
                            <ContentInfoConnectItem
                                key={suggestion.externalId}
                                contentHasBeenAdded={suggestion.contentHasBeenAdded}
                                title={suggestion.title}
                                posterImage={suggestion.posterImage}
                                overview={suggestion.overview}
                                releaseDate={suggestion.releaseDate}
                                buttons={
                                    <div>
                                        <button
                                            className={connectStyles.btnConnect}
                                            onClick={() => this.addContent(suggestion.externalId, false)}
                                        >
                                            {`Add ${this.state.type}`}
                                        </button>
                                        <button
                                            className={cls(connectStyles.btnConnect, styles.interestButton)}
                                            onClick={() => this.addContent(suggestion.externalId, true)}
                                        >
                                            <span>Add with</span>
                                            <img src="/img/icons/anticipated-yes.svg" />
                                        </button>
                                    </div>
                                }
                            />
                        ))
                    )}
                </div>
            </Dialog>
        );
    }
}

AutoAddContentDialog.dialogName = "DIALOG_AUTO_ADD_CONTENT";

AutoAddContentDialog.propTypes = {
    openDialog: PropTypes.func.isRequired,
    doSearch: PropTypes.func.isRequired,
    add: PropTypes.func.isRequired,
    loadContentInfo: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
};

export default withOpenDialog(
    connect(
        ({ search }) => ({
            params: search?.params || {},
        }),
        {
            doSearch,
            loadContentInfo: loadContentInfoSuggestionsByTitle,
            add: addFromExternal,
        },
    )(AutoAddContentDialog),
);
