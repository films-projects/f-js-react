import { postJson, post, getJson } from "../Common/util/defaultRequest";
import {
    ADD_CONTENT,
    ADD_CONTENT_FROM_EXTERNAL,
    DELETE_CONTENT,
    LOAD_UPCOMING_SERIES,
    SUGGEST_FILM,
    UPDATE_CONTENT,
} from "./endpoints";
import { FULL_CONTENT_INFO } from "../ExternalContent/constants/contentInfoEndpoints";

export const addReq = (type, params) => postJson(ADD_CONTENT(type), params);

export const addFromExternalReq = (type, externalId, interested) =>
    postJson(ADD_CONTENT_FROM_EXTERNAL(type, externalId, interested));

export const patchReq = ({ id, title, year }) => post(UPDATE_CONTENT(id, { title, year }));

export const deleteReq = id => post(DELETE_CONTENT(id));

export const setMyRatingReq = (contentId, rating) => post(UPDATE_CONTENT(contentId, { rating }));

export const setWatchingReq = (contentId, watching) => post(UPDATE_CONTENT(contentId, { watching }));

export const setSpecialInterestReq = (contentId, specialInterest) =>
    post(UPDATE_CONTENT(contentId, { specialInterest }));

export const getFullContentReq = contentId => getJson(FULL_CONTENT_INFO(contentId));

export const suggestFilm = params => getJson(SUGGEST_FILM(params));

export const loadUpcomingSeriesReq = () => getJson(LOAD_UPCOMING_SERIES);
