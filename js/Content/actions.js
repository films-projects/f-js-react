import { REMOVE_CONTENT, REPLACE_CONTENT, UPDATE_CONTENT } from "./actionTypes";
import {
    addFromExternalReq,
    addReq,
    deleteReq,
    patchReq,
    setSpecialInterestReq,
    setMyRatingReq,
    setWatchingReq,
    loadUpcomingSeriesReq,
} from "./requests";
import { closeDialogs, openDialog } from "../Common/actions/dialogActions";
import { addNotificationFromError, addSuccessNotification } from "../Common/actions/notificationActions";
import ConnectContentInfoDialog from "../ExternalContent/dialogs/ConnectContentInfoDialog";

export const removeFilm = id => ({
    type: REMOVE_CONTENT,
    content: id,
});

export const updateContent = film => ({
    type: UPDATE_CONTENT,
    content: film,
});

export const replaceContent = film => ({
    type: REPLACE_CONTENT,
    content: film,
});

export const add = (type, params) => dispatch =>
    addReq(type, params)
        .then(res => res.json())
        .then(({ id, title, type }) => {
            dispatch(addSuccessNotification(params.title + " has been added"));
            dispatch(openDialog(ConnectContentInfoDialog.dialogName, { id, title, type }));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const addFromExternal = (type, externalId, interested) => dispatch =>
    addFromExternalReq(type, externalId, interested)
        .then(res => res.json())
        .then(({ title }) => {
            dispatch(addSuccessNotification(title + " has been added"));
            dispatch(closeDialogs());
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const update = content => dispatch =>
    patchReq(content)
        .then(() => {
            dispatch(
                updateContent({
                    id: content.id,
                    title: content.title,
                    year: content.year,
                    watching: content.watching,
                }),
            );
            dispatch(closeDialogs());
            dispatch(addSuccessNotification(content.title + " has been updated"));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const deleteContent = film => dispatch =>
    deleteReq(film.id)
        .then(() => {
            dispatch(removeFilm(film.id));
            dispatch(closeDialogs());
            dispatch(addSuccessNotification(film.title + " has been deleted"));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const setMyRating = (contentId, value) => dispatch =>
    setMyRatingReq(contentId, value)
        .then(() => {
            const contentUpdate = { id: contentId, myRating: value, seen: value !== 0 };
            if (value !== 0) {
                contentUpdate.anticipated = false;
            }
            dispatch(updateContent(contentUpdate));
            dispatch(addSuccessNotification("The content has been updated"));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const setWatching = (contentId, value) => dispatch =>
    setWatchingReq(contentId, value)
        .then(() => {
            dispatch(updateContent({ id: contentId, watching: value }));
            dispatch(addSuccessNotification("The content has been updated"));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const setAnticipated = (contentId, value) => dispatch =>
    setSpecialInterestReq(contentId, value)
        .then(() => {
            dispatch(updateContent({ id: contentId, anticipated: value }));
            dispatch(addSuccessNotification("The content has been updated"));
        })
        .catch(e => dispatch(addNotificationFromError(e)));

export const loadUpcomingSeries = () => dispatch =>
    loadUpcomingSeriesReq().catch(e => dispatch(addNotificationFromError(e)));
