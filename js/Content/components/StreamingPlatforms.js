import React from "react";
import PropTypes from "prop-types";

import styles from "./StreamingPlatforms.scss";
import { watchOptionMap, friendlyNames } from "../../Common/constants/watchProviders";

const StreamingPlatforms = ({ contentId, streamingPlatforms }) => (
    <span className={styles.streamingPlatforms}>
        {Object.keys(watchOptionMap).map(
            provider =>
                streamingPlatforms.includes(provider) && (
                    <a href={`/watch/${contentId}/${provider}`} key={provider} target="_blank" rel="noreferrer">
                        <img src={`/img/external/${watchOptionMap[provider]}`} title={friendlyNames[provider]} />
                    </a>
                ),
        )}
    </span>
);

StreamingPlatforms.propTypes = {
    contentId: PropTypes.number,
    streamingPlatforms: PropTypes.array.isRequired,
};

StreamingPlatforms.defaultProps = {
    contentId: 0,
};

export default StreamingPlatforms;
