import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./TopSlide.scss";
import slideStyles from "../Slide.scss";

const TopSlide = ({ content }) => {
    if (content.posterImage) {
        const path = `url(${content.posterImage})`;
        return <div className={cls(styles.top, slideStyles.top)} style={{ backgroundImage: path }}></div>;
    } else {
        return <div className={cls(styles.top, slideStyles.top)} style={{ backgroundColor: "#555" }}></div>;
    }
};

TopSlide.propTypes = { content: PropTypes.object.isRequired };

export default TopSlide;
