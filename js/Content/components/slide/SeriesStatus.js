import React from "react";
import PropTypes from "prop-types";
import styles from "./SeriesStatus.scss";

const SeriesStatus = ({ series }) => {
    if (!series.status) {
        return <></>;
    }
    return (
        <>
            <span className={styles.middot}>&middot;</span>
            <span>{series.status}</span>
        </>
    );
};

SeriesStatus.propTypes = { series: PropTypes.object.isRequired };

export default SeriesStatus;
