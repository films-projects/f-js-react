import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./StatusIcons.scss";
import contentHeaderStyles from "./ContentHeaderMenu.scss";
import ContentHeaderMenu from "./ContentHeaderMenu";

const StatusIcons = ({ content, setMyRating }) => (
    <ContentHeaderMenu menuIconClassname={styles[`rating-${content.myRating}`]} dropdownClassname={styles.dropdown}>
        <div>
            {[0, -1].map(val => (
                <div
                    key={val}
                    className={contentHeaderStyles.dropdownIcon}
                    onClick={() => setMyRating(content.id, val)}
                >
                    <div className={cls(styles.icon, styles[`rating-${val}`])} />
                </div>
            ))}
        </div>
        {[1, 2, 3, 4, 5].map(val => (
            <div key={val} className={contentHeaderStyles.dropdownIcon} onClick={() => setMyRating(content.id, val)}>
                <div className={cls(styles.icon, styles[`rating-${val}`])} />
            </div>
        ))}
    </ContentHeaderMenu>
);

export const StatusIcon = ({ content, className }) => {
    const seenClassNames = cls(styles.icon, className, styles[`rating-${content.myRating}`]);
    return <div className={seenClassNames}></div>;
};

StatusIcon.propTypes = {
    content: PropTypes.object.isRequired,
    className: PropTypes.string,
};

StatusIcons.propTypes = {
    setMyRating: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
};

export default StatusIcons;
