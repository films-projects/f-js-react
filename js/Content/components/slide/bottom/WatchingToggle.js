import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./WatchingToggle.scss";
import contentHeaderStyles from "./ContentHeaderMenu.scss";
import ContentHeaderMenu from "./ContentHeaderMenu";

const AnticipatedToggle = ({ content, setWatching }) => {
    const menuIconClassname = cls({
        [styles.watchingNo]: !content.watching,
        [styles.watchingYes]: content.watching,
    });
    return (
        <ContentHeaderMenu
            menuIconClassname={menuIconClassname}
            dropdownClassname={styles.dropdown}
            menuClassname={styles.menu}
        >
            <div className={contentHeaderStyles.dropdownIcon} onClick={() => setWatching(content.id, false)}>
                <div className={cls(styles.icon, styles.watchingNo)} />
            </div>
            <div className={contentHeaderStyles.dropdownIcon} onClick={() => setWatching(content.id, true)}>
                <div className={cls(styles.icon, styles.watchingYes)} />
            </div>
        </ContentHeaderMenu>
    );
};

AnticipatedToggle.propTypes = {
    content: PropTypes.object.isRequired,
};

AnticipatedToggle.propTypes = {
    setWatching: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
};

export default AnticipatedToggle;
