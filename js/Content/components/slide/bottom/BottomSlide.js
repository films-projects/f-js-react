import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./BottomSlide.scss";
import StatusIcons from "../../../containers/StatusIconsContainer";
import AnticipatedToggle from "../../../containers/AnticipatedToggleContainer";
import WatchingToggle from "../../../containers/WatchingToggleContainer";
import ContentTools from "./ContentTools";

const BottomSlide = ({ content, open }) => (
    <div className={cls(styles.bottom, { [styles.open]: open })}>
        <div className={styles.infoHeader}>
            <ContentTools content={content} />
            <StatusIcons content={content} />
            {content.type === "series" && <WatchingToggle content={content} />}
            {(content.type === "film" || !content.watching) && <AnticipatedToggle content={content} />}
        </div>
        <hr />
        <div className={styles.plot}>{content.overview || "No info available"}</div>
    </div>
);

BottomSlide.propTypes = {
    content: PropTypes.object.isRequired,
    open: PropTypes.bool,
};

BottomSlide.defaultProps = {
    open: false,
};

export default BottomSlide;
