import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import withOpenDialog from "../../../../Common/HOC/WithOpenDialog";

import style from "./ContentHeaderMenu.scss";

const ContentHeaderMenu = ({ children, menuIconClassname, dropdownClassname, menuClassname, left }) => {
    const menuClass = cls({
        [style.menu]: true,
        [menuClassname]: true,
        [style.left]: left,
        [style.right]: !left,
    });
    return (
        <div className={menuClass}>
            <div className={style.menuHeader}>
                <div className={cls(menuIconClassname, style.menuIcon)}></div>
            </div>
            <div className={cls(style.dropdown, dropdownClassname)}>{children}</div>
        </div>
    );
};

ContentHeaderMenu.propTypes = {
    children: PropTypes.array.isRequired,
    menuIconClassname: PropTypes.string.isRequired,
    dropdownClassname: PropTypes.string,
    menuClassname: PropTypes.string,
    left: PropTypes.bool,
};

ContentHeaderMenu.defaultProps = {
    left: true,
};

export default withOpenDialog(ContentHeaderMenu);
