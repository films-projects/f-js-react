import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./AnticipatedToggle.scss";
import contentHeaderStyles from "./ContentHeaderMenu.scss";
import ContentHeaderMenu from "./ContentHeaderMenu";

const AnticipatedToggle = ({ content, setAnticipated }) => {
    if (content.seen) {
        return <></>;
    } else {
        const menuIconClassname = cls({
            [styles.anticipatedNo]: !content.anticipated,
            [styles.anticipatedYes]: content.anticipated,
        });
        return (
            <ContentHeaderMenu
                menuIconClassname={menuIconClassname}
                dropdownClassname={styles.dropdown}
                menuClassname={styles.menu}
            >
                <div className={contentHeaderStyles.dropdownIcon} onClick={() => setAnticipated(content.id, false)}>
                    <div className={cls(styles.icon, styles.anticipatedNo)} />
                </div>
                <div className={contentHeaderStyles.dropdownIcon} onClick={() => setAnticipated(content.id, true)}>
                    <div className={cls(styles.icon, styles.anticipatedYes)} />
                </div>
            </ContentHeaderMenu>
        );
    }
};

AnticipatedToggle.propTypes = {
    content: PropTypes.object.isRequired,
};

AnticipatedToggle.propTypes = {
    setAnticipated: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
};

export default AnticipatedToggle;
