import React from "react";
import PropTypes from "prop-types";

import withOpenDialog from "../../../../Common/HOC/WithOpenDialog";

import style from "./ContentTools.scss";
import ModifyContentDialog from "../../../dialogs/ModifyContentDialog";
import DeleteFilmDialog from "../../../dialogs/DeleteContentDialog";

const ContentTools = ({ content, openDialog }) => (
    <>
        <img
            className={style.contentToolIcon}
            src="/img/icons/delete.svg"
            onClick={() => {
                openDialog(DeleteFilmDialog.dialogName, { content });
            }}
        />
        <img
            className={style.contentToolIcon}
            src="/img/icons/edit.svg"
            onClick={() => {
                openDialog(ModifyContentDialog.dialogName, { content });
            }}
        />
    </>
);

ContentTools.propTypes = {
    content: PropTypes.object.isRequired,
    openDialog: PropTypes.func.isRequired,
};

export default withOpenDialog(ContentTools);
