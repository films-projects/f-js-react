import React, { useState } from "react";
import PropTypes from "prop-types";

import styles from "./SlideContainer.scss";
import Slide from "./Slide";
import Rating from "../Rating";
import StreamingPlatforms from "../StreamingPlatforms";
import ShowContentInfoDialog from "../../../ExternalContent/dialogs/ShowContentInfoDialog";
import withOpenDialog from "../../../Common/HOC/WithOpenDialog";
import ConnectContentInfoDialog from "../../../ExternalContent/dialogs/ConnectContentInfoDialog";
import SeriesStatus from "./SeriesStatus";

const SlideContainer = ({ content, openDialog }) => {
    const [open, setOpen] = useState(false);

    return (
        <div onClick={() => setOpen(!open)} className={styles.slideContainer}>
            <Slide content={content} open={open} />
            <div className={styles.contentSummary}>
                <div className={styles.title} title={content.title}>
                    {content.title}
                </div>
                <div>
                    <span className={styles.contentDetails}>{content.year}</span>
                    <SeriesStatus series={content} />
                </div>
                <div className={styles.streamingContainer}>
                    <StreamingPlatforms contentId={content.id} streamingPlatforms={content.streamingPlatforms} />
                </div>

                <img
                    className={styles.contentInfoButton}
                    src="/img/icons/info.svg"
                    title="Show content info"
                    onClick={e => {
                        e.stopPropagation();

                        content.imdbId
                            ? openDialog(ShowContentInfoDialog.dialogName, { contentId: content.id })
                            : openDialog(ConnectContentInfoDialog.dialogName, {
                                  title: content.title,
                                  id: content.id,
                                  type: content.type,
                              });
                    }}
                />

                <div className={styles.ratingsContainer}>
                    <Rating imdbRating={content.imdbRating} imdbId={content.imdbId} className={styles.rating} />
                </div>
            </div>
        </div>
    );
};

SlideContainer.propTypes = {
    content: PropTypes.object.isRequired,
    openDialog: PropTypes.func.isRequired,
};

export default withOpenDialog(SlideContainer);
