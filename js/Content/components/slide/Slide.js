import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import TopSlide from "./top/TopSlide";

import styles from "./Slide.scss";
import BottomSlide from "./bottom/BottomSlide";

const Slide = ({ content, open }) => (
    <div className={cls(styles.slide, { [styles.open]: open })}>
        <BottomSlide content={content} />
        <TopSlide content={content} />
    </div>
);

Slide.propTypes = {
    content: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
};

export default Slide;
