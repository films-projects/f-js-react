import React from "react";
import PropTypes from "prop-types";
import cls from "classnames";

import styles from "./rating.scss";

const IMDB_LINK = id => (id ? `https://www.imdb.com/title/${id}/` : undefined);
const Rating = ({ imdbRating, imdbId, className, ratingClassname }) =>
    imdbId ? (
        <span className={cls(styles.rating, className)}>
            <a href={IMDB_LINK(imdbId)} target="_blank" rel="noreferrer">
                <img src="/img/external/imdb.svg" />
            </a>
            <span className={cls(styles.value, ratingClassname)}>{imdbRating || "?"}</span>
        </span>
    ) : (
        <></>
    );

Rating.propTypes = {
    imdbRating: PropTypes.number,
    imdbId: PropTypes.string,
    className: PropTypes.string,
    ratingClassname: PropTypes.string,
};

Rating.defaultProps = {
    imdbRating: undefined,
    imdbId: undefined,
};

export default Rating;
