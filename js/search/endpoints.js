export const SEARCH_DO = "/search/do";
export const LOAD_SEARCH_FAVORITES_URL = "/search/favorites";
export const ADD_SEARCH_FAVORITES_URL = "/search/favorites/add";
export const DELETE_SEARCH_FAVORITES_URL = "/search/favorites/remove";
