import { connect } from "react-redux";

import SearchResults from "../components/SearchResults";

export default connect(
    ({
        search: {
            data: { content },
            isLoading,
        },
    }) => ({ isLoading, films: content }),
)(SearchResults);
