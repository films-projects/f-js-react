import { connect } from "react-redux";

import SearchForm from "../components/SearchForm";

import { doSearch, doAddSearchFavorite } from "../actions";

export default connect(state => ({ watchOptions: state.watchOptions }), {
    doSearch,
    addSearchFavorite: doAddSearchFavorite,
})(SearchForm);
