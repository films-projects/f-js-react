import { connect } from "react-redux";

import { doSearch } from "../actions";

import Pagination from "../components/Pagination";

export default connect(
    ({
        search: {
            data: { totalPages, number, totalElements },
            params,
        },
    }) => ({
        numPages: totalPages,
        currentPage: number,
        totalElements,
        params,
    }),
    { onClick: doSearch },
)(Pagination);
