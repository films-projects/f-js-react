import { getJson, postJson } from "../Common/util/defaultRequest";
import {
    ADD_SEARCH_FAVORITES_URL,
    DELETE_SEARCH_FAVORITES_URL,
    LOAD_SEARCH_FAVORITES_URL,
    SEARCH_DO,
} from "./endpoints";
import queryString from "query-string";

export const searchReq = (params, page = null) => {
    if (page !== null) {
        params.page = page;
    }
    params.size = 24;
    const url = SEARCH_DO + "?" + queryString.stringify(params);
    return getJson(url, params);
};

export const loadSearchFavoritesReq = () => getJson(LOAD_SEARCH_FAVORITES_URL);

export const addSearchFavoritesReq = query =>
    postJson(ADD_SEARCH_FAVORITES_URL, { query: query }).then(res => res.json());

export const deleteSearchFavoritesReq = query =>
    postJson(DELETE_SEARCH_FAVORITES_URL, { query: query }).then(res => res.json());
