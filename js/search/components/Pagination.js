import React, { useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";
import PropTypes from "prop-types";
import cls from "classnames";
import queryString from "query-string";

import styles from "./Pagination.scss";

const Pagination = ({ numPages, currentPage, onClick, params, totalElements }) => {
    const actualPage = currentPage + 1;
    const [goToPage, setGoToPage] = useState(0);
    function doSearch(page) {
        const qs = Object.assign({}, queryString.parse(location.search), { page });
        window.history.replaceState(null, null, "/search?" + queryString.stringify(qs));
        onClick(params, page);
    }
    useHotkeys("Comma", () => {
        if (actualPage > 1) doSearch(currentPage - 1);
    });
    useHotkeys("Period", () => {
        if (actualPage < numPages) doSearch(currentPage + 1);
    });

    function switchToPage() {
        const page = goToPage - 1;
        if (page && page > 0 && page <= numPages) {
            const qs = Object.assign({}, queryString.parse(location.search), { page });
            window.history.replaceState(null, null, "/search?" + queryString.stringify(qs));
            onClick(params, page);
        }
    }

    return (
        <div className={styles.pagination}>
            <div className={styles.pages}>
                <button
                    className={cls(styles.paginationElement, styles.pageButton)}
                    onClick={() => doSearch(currentPage - 1)}
                    disabled={actualPage === 1}
                >
                    {"<"}
                </button>
                <div className={cls(styles.paginationElement, styles.paginationCount)}>{totalElements} results</div>
                <div className={cls(styles.paginationElement, styles.paginationCount)}>
                    Page {actualPage} of {numPages}
                </div>
                {numPages > 2 && (
                    <div className={cls(styles.paginationElement, styles.paginationCount)}>
                        <span>Page</span>
                        <input type="number" min={1} max={numPages} onChange={e => setGoToPage(e.target.value)}></input>

                        <button
                            className={styles.pageButton}
                            onClick={() => switchToPage()}
                            disabled={actualPage === numPages}
                        >
                            Go
                        </button>
                    </div>
                )}
                <button
                    className={cls(styles.paginationElement, styles.pageButton)}
                    onClick={() => doSearch(currentPage + 1)}
                    disabled={actualPage === numPages}
                >
                    {">"}
                </button>
            </div>
        </div>
    );
};

Pagination.propTypes = {
    totalElements: PropTypes.number,
    numPages: PropTypes.number,
    currentPage: PropTypes.number,
    params: PropTypes.object,
    onClick: PropTypes.func.isRequired,
};

Pagination.defaultProps = {
    totalElements: 0,
    numPages: 0,
    currentPage: 0,
};

export default Pagination;
