import React from "react";
import PropTypes from "prop-types";

import styles from "./ResultContainer.scss";
import LoadingBar from "../../Common/components/LoadingBar";
import SlideContainer from "../../Content/components/slide/SlideContainer";
import Pagination from "../containers/PaginationContainer";

const SearchResults = ({ films, isLoading }) =>
    isLoading ? (
        <LoadingBar />
    ) : (
        <>
            <Pagination />
            <div className={styles.resultsWrapper}>
                <div className={styles.container}>
                    {films.map(content => (
                        <SlideContainer key={content.id} content={content} />
                    ))}{" "}
                </div>
            </div>
            <Pagination />
        </>
    );

SearchResults.propTypes = {
    films: PropTypes.array.isRequired,
    isLoading: PropTypes.bool,
};

SearchResults.defaultProps = { isLoading: false, films: [] };

export default SearchResults;
