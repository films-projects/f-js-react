import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import LoadingBar from "../../Common/components/LoadingBar";

import { doLoadSearchFavorites, doDeleteSearchFavorite } from "../actions";

import styles from "./SearchHistory.scss";

const Pill = ({ field, value }) => (
    <span className={styles.pill}>
        <span className={styles.pillField}>{field}</span>
        <span className={styles.pillValue}>{value.join(", ")}</span>
    </span>
);

Pill.propTypes = {
    field: PropTypes.string.isRequired,
    value: PropTypes.array.isRequired,
};

class SearchHistory extends React.Component {
    componentDidMount() {
        if (!this.props.loaded) {
            this.props.loadFavorites();
        }
    }

    render() {
        const { loading, favorites, removeFavorite } = this.props;
        if (loading) {
            return <LoadingBar />;
        }
        if (favorites.length < 1) {
            return <div className={styles.noSavedQueries}>You have not saved any queries yet</div>;
        }
        return (
            <div>
                {favorites.map((fav, idx) => {
                    const params = fav.params;
                    return (
                        <div key={idx} className={styles.query}>
                            <img
                                className={styles.queryDeleteButton}
                                src="/img/icons/delete.svg"
                                onClick={() => removeFavorite(fav.query)}
                            />
                            <a href={`/search?${fav.query}`}>
                                {Object.keys(params).map(key => (
                                    <Pill className={styles.queryParam} key={key} field={key} value={params[key]} />
                                ))}
                            </a>
                        </div>
                    );
                })}
            </div>
        );
    }
}

SearchHistory.propTypes = {
    loading: PropTypes.bool.isRequired,
    favorites: PropTypes.array.isRequired,
    loaded: PropTypes.bool.isRequired,
    loadFavorites: PropTypes.func.isRequired,
    removeFavorite: PropTypes.func.isRequired,
};

export default connect(({ searchFavorites: { loading, favorites, loaded } }) => ({ loading, favorites, loaded }), {
    loadFavorites: doLoadSearchFavorites,
    removeFavorite: doDeleteSearchFavorite,
})(SearchHistory);
