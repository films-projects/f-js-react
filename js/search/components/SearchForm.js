import React from "react";
import debounce from "debounce";
import PropTypes from "prop-types";
import queryString from "query-string";
import cls from "classnames";

import Row from "../../Common/components/form/Row";
import { selRTL, txt } from "../../Common/util/createInput";
import getAllowedYears, { getYears } from "../../Common/util/getAllowedYears";

import { statusMap } from "../../Common/constants/seriesStatusValues";
const statusValues = Object.assign({ all: "Any" }, statusMap);

import styles from "./SearchForm.scss";
import { watchOptionMap } from "../../Common/constants/watchProviders";
import SearchHistory from "./SearchHistory";

const defaultValues = {
    yearFrom: getYears()[getYears().length - 1],
    yearTo: getYears()[0],
    orderBy: "title",
    order: "asc",
    sort: "title,asc",
    type: "all",
    title: "",
    minRating: "",
    page: 0,
    myRating: [],
    watch: [],
    status: "all",
    showFavorites: false,
    anticipated: false,
};

const ignoreParams = ["order", "orderBy", "open", "size", "showFavorites"];

const openParams = [
    "minRating",
    "myRating",
    "sort",
    "type",
    "yearFrom",
    "yearTo",
    "anticipated",
    "status",
    "released",
    "watch",
];

class SearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.filterState = this.filterState.bind(this);

        const { sort } = queryString.parse(location.search);
        const sortParams = {};

        if (sort) {
            const parts = sort.split(",");
            sortParams["orderBy"] = parts[0];
            sortParams["order"] = parts[1];
        }

        const params = queryString.parse(location.search);

        if ("myRating" in params) {
            if (params.myRating instanceof Array) {
                params.myRating = params.myRating.map(it => Number(it));
            } else {
                params.myRating = [Number(params.myRating)];
            }
        }

        if ("watch" in params && !(params.watch instanceof Array)) {
            params.watch = [params.watch];
        }

        let openForm = false;
        Object.keys(params).forEach(key => {
            if (openParams.includes(key)) {
                openForm = true;
                return false;
            }
        });
        this.state = Object.assign({ open: openForm }, defaultValues, sortParams, params);

        this.doSearch = debounce(props.doSearch, 500);
        this.search = this.search.bind(this);
        this.setSort = this.setSort.bind(this);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFavorites = this.toggleFavorites.bind(this);
        this.setRating = this.setRating.bind(this);
        this.setWatching = this.setWatching.bind(this);
        this.setInterested = this.setInterested.bind(this);
        this.setFilmReleased = this.setFilmReleased.bind(this);
        this.setWatchOptions = this.setWatchOptions.bind(this);

        this.resetState = this.resetState.bind(this);

        this.sortFieldRef = React.createRef();
        this.sortDirectionRef = React.createRef();
    }

    componentDidMount() {
        this.search();
    }

    resetState() {
        this.setState(defaultValues, this.search);
    }

    filterState() {
        const searchableState = Object.assign({}, this.state);
        ignoreParams.forEach(i => {
            delete searchableState[i];
        });
        Object.keys(defaultValues)
            .filter(n => {
                if (Array.isArray(defaultValues[n])) {
                    return searchableState[n].length === 0;
                } else {
                    return searchableState[n] === defaultValues[n];
                }
            })
            .forEach(n => delete searchableState[n]);
        return searchableState;
    }

    toggleForm() {
        const { open } = this.state;
        if (open) {
            this.setState({ open: false, showFavorites: false });
        } else {
            this.setState({ open: true });
        }
    }

    toggleFavorites() {
        const { showFavorites } = this.state;
        this.setState({ showFavorites: !showFavorites });
    }

    search() {
        const filtered = this.filterState();
        const query = queryString.stringify(filtered);
        window.history.pushState(null, null, "/search?" + query);
        this.doSearch(filtered);
    }

    setRating(val) {
        if (this.state.myRating.includes(val)) {
            this.setState({ myRating: this.state.myRating.filter(it => it !== val) }, this.search);
        } else {
            this.setState({ myRating: [...this.state.myRating, val] }, this.search);
        }
    }

    setWatchOptions(val) {
        let newArray;
        if (this.state.watch.includes(val)) {
            newArray = this.state.watch.filter(it => it !== val);
        } else {
            newArray = [...this.state.watch, val];
        }
        if (newArray.length > 0) {
            this.setState({ watch: newArray }, this.search);
        } else {
            this.setState({ watch: [] }, this.search);
        }
    }

    setWatching(val) {
        if (this.state.watching === val) {
            this.setState({ watching: undefined }, this.search);
        } else {
            this.setState({ watching: val }, this.search);
        }
    }

    setInterested(val) {
        if (this.state.anticipated === val) {
            this.setState({ anticipated: undefined }, this.search);
        } else {
            this.setState({ anticipated: val }, this.search);
        }
    }

    setFilmReleased(val) {
        if (this.state.released === val) {
            this.setState({ released: undefined }, this.search);
        } else {
            this.setState({ released: val }, this.search);
        }
    }

    setSort() {
        const sortField = this.sortFieldRef.current.value;
        const sortDirection = this.sortDirectionRef.current.value;
        this.setState(
            {
                order: sortDirection,
                orderBy: sortField,
                sort: `${sortField},${sortDirection}`,
            },
            this.search,
        );
    }

    addSearchFavorite() {
        const filtered = this.filterState();
        const query = queryString.stringify(filtered);
        this.props.addSearchFavorite(query);
    }

    render() {
        const { watchOptions } = this.props;
        const createChange = name => e => {
            this.setState({ [name]: e.target.value, page: 0 }, this.search);
        };
        const yearMap = {};
        getYears().forEach(year => (yearMap[year + ""] = year));

        const hasSearchParams = Object.keys(this.filterState()).length > 0;

        const orderMap = {
            asc: "Asc",
            desc: "Desc",
        };

        const orderByMap = {
            title: "Title",
            rating: "Rating",
            year: "Year",
        };

        const typeMap = {
            all: "All",
            film: "Films",
            series: "Series",
        };

        const {
            title,
            yearFrom,
            yearTo,
            myRating,
            orderBy,
            order,
            type,
            watching,
            minRating,
            open,
            anticipated,
            status,
            released,
            watch,
            showFavorites,
        } = this.state;

        const hideExtraField = !open;

        if (hideExtraField) {
            return (
                <div className={cls(styles.formWrapper, styles.collapsed)}>
                    <div className={styles.collapsedRow}>{txt(title, createChange("title"), "(Part of) title")}</div>
                    <div className={styles.moreOptions}>
                        <span className={styles.searchOption} onClick={() => this.toggleFavorites()}>
                            {showFavorites ? "Hide saved queries" : "Show saved queries"}
                        </span>
                        <div className={cls(styles.searchToolGroupExpand)}></div>
                        <div>
                            <span className={styles.searchOption} onClick={() => this.toggleForm()}>
                                More options
                            </span>
                        </div>
                    </div>

                    {!open && showFavorites && <SearchHistory />}
                </div>
            );
        }

        return (
            <div className={styles.formWrapper}>
                <div className={styles.form}>
                    <Row>
                        <label htmlFor="input-title">Title</label>
                        <div className={styles.halfwrapper}>
                            {txt(title, createChange("title"), "(Part of) title")}
                            {selRTL(type, createChange("type"), typeMap, null, "type")}
                        </div>
                    </Row>

                    <Row>
                        <label>Year</label>
                        <div className={styles.halfwrapper}>
                            <select dir="rtl" onChange={createChange("yearFrom")} value={yearFrom}>
                                {getAllowedYears}
                            </select>
                            <select dir="rtl" onChange={createChange("yearTo")} value={yearTo}>
                                {getAllowedYears}
                            </select>
                        </div>
                    </Row>

                    <Row>
                        <label>Interested</label>
                        <div className={styles.iconRow}>
                            {["no", "yes"].map(val => {
                                return (
                                    <div
                                        key={val}
                                        className={cls({
                                            [styles.icon]: true,
                                            [styles[`interested-${val}`]]: true,
                                            [styles.selected]: anticipated == val,
                                        })}
                                        onClick={() => this.setInterested(val)}
                                    />
                                );
                            })}
                        </div>
                    </Row>

                    <Row>
                        <label>My Rating</label>
                        <div className={styles.iconRow}>
                            {[0, -1, 1, 2, 3, 4, 5].map(val => {
                                return (
                                    <div
                                        key={val}
                                        className={cls({
                                            [styles.icon]: true,
                                            [styles[`rating-${val}`]]: true,
                                            [styles.selected]: myRating.includes(val),
                                        })}
                                        onClick={() => this.setRating(val)}
                                    />
                                );
                            })}
                        </div>
                    </Row>

                    <Row className={styles.imdbRatingRow}>
                        <label>IMDb rating</label>
                        <input
                            type="number"
                            min="0"
                            max="9.5"
                            step="0.5"
                            onChange={createChange("minRating")}
                            value={minRating}
                            name="minRating"
                            placeholder="Minimum rating"
                        />
                    </Row>

                    <Row>
                        <label>Sort by</label>
                        <div className={styles.halfwrapper}>
                            <select dir="rtl" onChange={() => this.setSort()} value={orderBy} ref={this.sortFieldRef}>
                                {Object.keys(orderByMap).map(k => (
                                    <option key={k} value={k}>
                                        {orderByMap[k]}
                                    </option>
                                ))}
                            </select>
                            <select dir="rtl" onChange={() => this.setSort()} value={order} ref={this.sortDirectionRef}>
                                {Object.keys(orderMap).map(k => (
                                    <option key={k} value={k}>
                                        {orderMap[k]}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </Row>

                    {watchOptions.length > 0 && (
                        <Row>
                            <label>Watch on</label>
                            <div className={styles.iconRow}>
                                {watchOptions.map(op => (
                                    <div
                                        key={op}
                                        className={cls({
                                            [styles.icon]: true,
                                            [styles.selected]: watch.includes(op),
                                        })}
                                        onClick={() => this.setWatchOptions(op)}
                                    >
                                        <img src={"/img/external/" + watchOptionMap[op]} />
                                    </div>
                                ))}
                            </div>
                        </Row>
                    )}

                    {type === "series" && (
                        <Row>
                            <label>Series</label>
                            <div>
                                {["no", "yes"].map(val => (
                                    <div
                                        key={val}
                                        className={cls({
                                            [styles.icon]: true,
                                            [styles[`watching-${val}`]]: true,
                                            [styles.selected]: watching == val,
                                        })}
                                        onClick={() => this.setWatching(val)}
                                    />
                                ))}
                            </div>
                            {selRTL(status, createChange("status"), statusValues)}
                        </Row>
                    )}

                    {type === "film" && (
                        <Row>
                            <label>Film</label>
                            <div className={styles.iconRow}>
                                {[
                                    { key: "true", text: "Released" },
                                    { key: "false", text: "Upcoming" },
                                ].map(val => (
                                    <div
                                        key={val.key}
                                        className={cls({
                                            [styles.icon]: true,
                                            [styles.selected]: released === val.key,
                                        })}
                                        onClick={() => this.setFilmReleased(val.key)}
                                    >
                                        {val.text}
                                    </div>
                                ))}
                            </div>
                        </Row>
                    )}
                </div>
                <div className={styles.searchTools}>
                    <div className={styles.searchToolGroup}>
                        <span
                            className={cls(styles.searchOption, styles.withMarginRight)}
                            onClick={() => this.toggleFavorites()}
                        >
                            {showFavorites ? "Hide saved queries" : "Show saved queries"}
                        </span>
                        {hasSearchParams && (
                            <span className={styles.searchOption} onClick={() => this.addSearchFavorite()}>
                                Save this query
                            </span>
                        )}
                    </div>

                    <div className={cls(styles.searchToolGroupExpand)}></div>

                    <div className={styles.searchToolGroup}>
                        {hasSearchParams && (
                            <span
                                className={cls(styles.searchOption, styles.withMarginRight)}
                                onClick={() => this.resetState()}
                            >
                                Reset
                            </span>
                        )}
                        <span className={styles.searchOption} onClick={() => this.toggleForm()}>
                            Show less options
                        </span>
                    </div>
                </div>
                <div>{open && showFavorites && <SearchHistory />}</div>
            </div>
        );
    }
}

SearchForm.propTypes = {
    doSearch: PropTypes.func.isRequired,
    addSearchFavorite: PropTypes.func.isRequired,
    watchOptions: PropTypes.array.isRequired,
};

export default SearchForm;
