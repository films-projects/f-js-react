import {
    ADD_SEARCH_FAVORITE,
    LOAD_SEARCH_FAVORITES,
    PERFORM_SEARCH,
    PERFORM_SEARCH_DONE,
    STORE_SEARCH_FAVORITES,
} from "./actionTypes";

import { addSearchFavoritesReq, loadSearchFavoritesReq, searchReq, deleteSearchFavoritesReq } from "./requests";
import { addNotificationFromError, addSuccessNotification } from "../Common/actions/notificationActions";

export const performSearch = params => ({
    type: PERFORM_SEARCH,
    content: params,
});

export const performSearchDone = json => ({
    type: PERFORM_SEARCH_DONE,
    content: json,
});

export const loadSearchFavorites = () => ({
    type: LOAD_SEARCH_FAVORITES,
});

export const addSearchFavorite = json => ({
    type: ADD_SEARCH_FAVORITE,
    content: json,
});

export const storeSearchFavorites = json => ({
    type: STORE_SEARCH_FAVORITES,
    content: json,
});

export const doSearch =
    (params, page = null) =>
    dispatch => {
        dispatch(performSearch(params));
        searchReq(params, page)
            .then(json => dispatch(performSearchDone(json)))
            .catch(e => {
                dispatch(performSearchDone([]));
                dispatch(addNotificationFromError(e));
            });
    };

export const doLoadSearchFavorites = () => dispatch => {
    dispatch(loadSearchFavorites());
    loadSearchFavoritesReq()
        .then(json => dispatch(storeSearchFavorites(json)))
        .catch(e => {
            dispatch(storeSearchFavorites([]));
            dispatch(addNotificationFromError(e));
        });
};

export const doAddSearchFavorite = query => dispatch => {
    addSearchFavoritesReq(query)
        .then(json => {
            dispatch(addSearchFavorite(json));
            dispatch(addSuccessNotification("Added favorite query"));
        })
        .catch(e => {
            dispatch(addNotificationFromError(e));
        });
};

export const doDeleteSearchFavorite = query => dispatch => {
    deleteSearchFavoritesReq(query)
        .then(json => {
            dispatch(storeSearchFavorites(json));
            dispatch(addSuccessNotification("Removed favorite query"));
        })
        .catch(e => {
            dispatch(addNotificationFromError(e));
        });
};
