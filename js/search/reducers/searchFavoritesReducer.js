import { LOAD_SEARCH_FAVORITES, STORE_SEARCH_FAVORITES, ADD_SEARCH_FAVORITE } from "../actionTypes";

export default (state = { loading: false, favorites: [], loaded: false }, { type, content }) => {
    switch (type) {
        case LOAD_SEARCH_FAVORITES:
            return { loading: true, favorites: [], loaded: false };
        case STORE_SEARCH_FAVORITES:
            return { loading: false, favorites: content, loaded: true };
        case ADD_SEARCH_FAVORITE:
            if (state.loaded) {
                return { loading: false, favorites: [...state.favorites, content], loaded: true };
            } else {
                return state;
            }
        default:
            return state;
    }
};
