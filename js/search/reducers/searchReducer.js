import { PERFORM_SEARCH, PERFORM_SEARCH_DONE } from "../actionTypes";

import { REMOVE_CONTENT, REPLACE_CONTENT, UPDATE_CONTENT } from "../../Content/actionTypes";

export default (state = { isLoading: true, data: { content: [] }, timestamp: 0 }, { type, content }) => {
    switch (type) {
        case PERFORM_SEARCH_DONE:
            if (content.timestamp >= state.timestamp) {
                return Object.assign({}, state, {
                    isLoading: false,
                    data: content.results,
                });
            } else {
                return state;
            }
        case PERFORM_SEARCH:
            return Object.assign({}, state, {
                isLoading: true,
                data: Object.assign({}, state.data, { number: 1, content: [] }),
                params: Object.assign({}, content),
            });
        case REMOVE_CONTENT: {
            const films = state.data.content.filter(film => film.id !== content);
            return Object.assign({}, state, {
                data: {
                    content: films,
                    number: state.data.number,
                    totalPages: state.data.totalPages,
                    totalElements: state.data.totalElements - 1,
                },
            });
        }
        case UPDATE_CONTENT: {
            const films = state.data.content.map(film =>
                film.id === content.id ? Object.assign({}, film, content) : film,
            );

            return Object.assign({}, state, {
                data: {
                    content: films,
                    number: state.data.number,
                    totalPages: state.data.totalPages,
                    totalElements: state.data.totalElements,
                },
            });
        }
        case REPLACE_CONTENT: {
            const films = state.data.content.map(film => (film.id === content.id ? content : film));

            return Object.assign({}, state, {
                data: {
                    content: films,
                    number: state.data.number,
                    totalPages: state.data.totalPages,
                    totalElements: state.data.totalElements,
                },
            });
        }
        default:
            return state;
    }
};
