import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import SearchContainer from "./containers/SearchFormContainer";

import SlideContainer from "./containers/SearchResultsContainer";
import styles from "./SearchPage.scss";
import { retrieveWatchOptions } from "../Common/WatchOptions/actions";

class SearchPage extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.loadWatchOptions();
    }

    render() {
        return (
            <>
                <SearchContainer />
                <SlideContainer />
                <div className={styles.buffer}></div>
            </>
        );
    }
}

SearchPage.propTypes = { loadWatchOptions: PropTypes.func.isRequired };

export default connect(undefined, { loadWatchOptions: retrieveWatchOptions })(SearchPage);
