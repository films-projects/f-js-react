import React from "react";

import styles from "./login.scss";

const Body = () => (
    <div className={styles.wrapper}>
        <form action="/login" method="post" className={styles.centered}>
            <input type="text" name="username" placeholder="Username" className={styles.username} />
            <input type="password" name="password" placeholder="Password" className={styles.password} />
            <label>Stay logged in</label>
            <input type="checkbox" name="rememberme" />
            <input type="submit" value="Log in" className={styles.submit} />
        </form>
    </div>
);

export default Body;
