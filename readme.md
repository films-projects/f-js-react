# Films frontend

Frontend to be used by the films backend applications:

-   [Films](https://gitlab.com/hgjkant/films-spring)
-   [Films Kotlin](https://gitlab.com/hgjkant/films-spring-kt)

Also see

-   [endpoints](docs/endpoints.md) for an overview of the endpoints called by the frontend

## Requirements

-   Node.js v16

## Usage

Use this frontend by

1. copying the example `webpack.config.js.example` and `.babelrc.example` the root of your project
2. remove the `.example` suffixes
3. setting the correct source and output dir in `webpack.config.js`
4. installing dependencies with `npm install` by pointing to the `package.json` provided in the frontend, for example with
   `npm i ./frontend/config`. This creates a `package.json` in your root dir.
5. copy the `scripts` from the example `package.json` to the one in your root dir.
6. compile the frontend using either
    - `npm run dev` to watch for changes and recompile automatically
    - `npm run build` when creating an artifact to deploy

## Background

The frontend can stretch a `div` with background image, and apply a blur to it. The `div` with the background image is
expected to have a `class="bg-image"`, and to blur it this outer div should contain another div with `class="bg-blur"`.

## Required media

The frontend refers to some images which are expected to be provided by the backend. [media.md](docs/media.md) provides a
list of these images, and in the `/img` example images are given which can be used, but other may be used as well as
long as have the same name and extension.

## Prettier

The frontend files can be formatted using `prettier`. Example configuration files can be found in the
[examples](./docs/examples) folder. Copy `.prettierrc` and `.prettierignore` to the root dir of the project. Use

```
npx prettier . --check
```

to check all files, or

```
npx prettier . --write
```

to update where necessary. Prettier can also
work with the IDE to write on save, for setup see [here](https://prettier.io/docs/en/editors).

### ESlint

You can use `ESLint` to lint `.js` files. The [examples](./examples) folder contains a example `.eslistrc`
configuration file which you can copy into the root dir of the project, and with

```
npx eslint frontend/js/**/**.js
```

you can check all the js files, if `frontend/` if the root dir of the frontend module

In IntelliJ you can enable linting with ESLint to directly see errors in the file.

### Stylelint

You can use `Stylelint` to lint `.scss` files. The [examples](./examples) folder contains an example
`.stylelintrc.json` configuration file which you can copy into the root dir of the project, and with

```
npx stylelint "**/*.scss"
```

you can check for errors, or use

```
npx stylelint "**/*.scss" --fix
```

to automatically fix them.

In IntelliJ you can enable linting with Stylelint to directly see errors in the file.

### Pre-commit hook

You can run all linting in a pre-commit hook before doing a commit. An example of this file can be found in
[the examples folder](docs/examples/pre-commit). You can put this file in `.git/modules/frontend/hooks` of the main
application's root dir, assuming this frontend project is a module in that project. The `.git` folder is likely hidden,
so you'll have to go there manually. If a `pre-commit` file already exists, you'll have to merge the contents of the
example with the existing file.
